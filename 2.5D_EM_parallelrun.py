from random import * 
import os
from pytictoc import TicToc

t = TicToc() 
t.tic() 					# Start timer

Sigma 	= [] 				# conductivity of the middle layer
Sigma_u = [] 				# conductivity of the upper layer
Sigma_l = [] 				# conductivity of the lower layer
Theta 	= [] 				# trajectory angle (degree)
Du 		= [] 				# distance of the tool center from the upper layer
Dl 		= [] 				# distance of the tool center from the lower layer
Caseno 	= []				# case number


filename = "cases.f90"		# we write our random set in a file for future use
f = open(filename,"w+")


samples = 100000

for x in xrange(0,samples):
	log_sigma 	= uniform(-2.0, 0.0)			# we have a uniform random sampling for log10(sigma)
	log_sigma_u = uniform(-2.0, 0.0)			# sigmas are between 0.01 and 1.0
	log_sigma_l = uniform(-2.0, 0.0)		
	theta 		= uniform(80.0, 100.0)			
	log_du 		= uniform(-2.0, 1.0)			# du and dl between 0.01 and 10
	log_dl 		= uniform(-2.0, 1.0)		
	# using append option, we put our random values in vector arrays for any potential use in future
	Sigma.append(round(10**log_sigma,2)); 		# we round sigma values to 2 decimals
	Sigma_u.append(round(10**log_sigma_u,2)); 	
	Sigma_l.append(round(10**log_sigma_l,2)); 	
	Theta.append(round(theta,1)); 
	Du.append(round(10**log_du,2)); 	
	Dl.append(round(10**log_dl,2)); 	
	Caseno.append(round(x,0)); 
	# writing to a file
	f.write(" %1.1f  %1.2f  %1.2f  %1.2f  %1.1f  %1.2f  %1.2f\n" %\
		 (Caseno[x],Sigma[x],Sigma_u[x],Sigma_l[x],Theta[x],Du[x],Dl[x]))

f.close()

# now we create our run command
ncpu = 20						# number of cpus per run
runs = samples/ncpu				# number of runs
i	 = 0

for y in xrange(0,runs):	
	run = "$pwd" 				# I add this useless command just because the run command cannot start with & (as we have in the "for" loop)
	for x in xrange(0,ncpu):	
 		run = run+" & bash run-riga-em.sh "\
 				+str(Sigma[i])+" "+str(Sigma_u[i])+" "+str(Sigma_l[i])+" "\
 				+str(Theta[i])+" "+str(Du[i])+" "+str(Dl[i])+" "\
 				+str(Caseno[i])
 		i = i+1
	os.system(run)

t.toc() 