import glob,sys
import numpy         as np
import pylab         as plt
from   igakit.io     import PetIGA,VTK
from   igakit.nurbs  import NURBS
from   igakit.igalib import bsp,iga
from   scipy         import stats
from   scipy.special import kv
from   colorama      import Fore, Back, Style 

# ============================================
def isfloat(value):
  try:
    float(value)
    return True
  except ValueError:
    return False

def ispi(value):
  if value=='pi':
	return np.pi
  else:
    return 1.0

def convert2num(str):
  aux  = 1.0
  sstr = np.size(str.split("*"))
  for i in range(sstr):
  	line = str.split("*")[i]
  	if isfloat(line):
  		aux = aux*float(line)
  	else: 
  		aux = aux*ispi(line)
  return aux

# ============================================
# Read FE point value
def readfiles(namefile):
  #
  seg  = []
  Azz  = []
  Pzz = []
  #
  f  = open(namefile)
  next(f) # Skip first line
  for fl,line in enumerate(f):   
    seg.append(float(line.split()[0]));
    Azz.append(float(line.split()[6]));
    Pzz.append(float(line.split()[7]));
  #
  return np.asarray(seg),np.asarray(Azz),np.asarray(Pzz)



# ========================================================================#
#                                 Main Code                               #

########################### - Read from terminal - ########################

# Read data
d       = int(sys.argv[1]) # dimension
Nkn     = int(sys.argv[2]) # Number on knots
p       = int(sys.argv[3]) # polynomial degree
#
nlevel  = int(sys.argv[4]) # number of patition levels (use 0 for IGA) e.g., 0,1,2,...
nbetas  = int(sys.argv[5]) # Fourier discretization of the y direction (number of betas) e.g., 0,1,2,...
npnts   = int(sys.argv[6]) # Number of receivers (per subdomain) e.g., 1,2,3,...
#
sigma   = convert2num(str(sys.argv[7])) # sigma_
# 
sub     = int(sys.argv[8]) # subtrajectory

########################### - Read from file - ############################

dirf  = "APzz/"
namef = "APzz_"
middlef1 = "N"+str(Nkn)+"_p"+str(p+1)+"_l"+str(nlevel)+"_Pos"+str(npnts)+"_Src"+str(1)+"_sigma"+str(sigma)+"_sub"+str(sub)
middlef2 = "N"+str(Nkn)+"_p"+str(p+1)+"_l"+str(nlevel)+"_Pos"+str(npnts)+"_Src"+str(2)+"_sigma"+str(sigma)+"_sub"+str(sub)
extf  = ".txt"

filename1 = dirf+namef+middlef1+extf 
filename2 = dirf+namef+middlef2+extf 

seg, Azz1, Pzz1 = readfiles(filename1)
seg, Azz2, Pzz2 = readfiles(filename2)

Azz = (Azz1+Azz2)/2
Pzz = (Pzz1+Pzz2)/2


################## - Write Azz & Pzz values in a file - ##################

dirf  = "APzz_2_source/"
namef = "APzz_"
extf  = ".f90"

middlef = "N"+str(Nkn)+"_p"+str(p+1)+"_l"+str(nlevel)+"_Pos"+str(npnts)+"_sigma"+str(sigma)+"_sub"+str(sub)

filename = dirf+namef+middlef+extf

nsg = len(seg)-1

if nsg == 0:
  f = open(filename,"w+")
  f.write("Segment|        Azz                Pzz\n")
  f.close()
f = open(filename,"a")
f.write("  %d    | %1.16f  %1.16f\n" % (seg[nsg],Azz[nsg],Pzz[nsg]))
f.close()


print "file "+filename+" : "+Fore.GREEN+"Data correctly written to file"+Fore.RESET
