
ALL: ${TARGETS}
clean::
	-@${RM} ${TARGETS}

topdir := $(shell cd .. && pwd)
PETIGAM_DIR ?= $(topdir)
include ${PETIGAM_DIR}/lib/petigam/conf/variables
include ${PETIGAM_DIR}/lib/petigam/conf/rules

2.5D_EM: 2.5D_EM.o chkopts
	${CLINKER} -o $@ $< ${PETIGAM_LIB}
	${RM} $<
