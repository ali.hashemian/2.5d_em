import glob,sys
import numpy         as np
import pylab         as plt
from   igakit.io     import PetIGA,VTK
from   igakit.nurbs  import NURBS
from   igakit.igalib import bsp,iga
from   scipy         import stats
from   scipy.special import kv
from   colorama      import Fore, Back, Style 

def isfloat(value):
  try:
    float(value)
    return True
  except ValueError:
    return False

def ispi(value):
  if value=='pi':
	return np.pi
  else:
    return 1.0

def convert2num(str):
  aux  = 1.0
  sstr = np.size(str.split("*"))
  for i in range(sstr):
  	line = str.split("*")[i]
  	if isfloat(line):
  		aux = aux*float(line)
  	else: 
  		aux = aux*ispi(line)
  return aux

# ============================================
# Equation to compute the exact value of Hzz
def HZZ(point,srcloc,srcmag,prm_user,case):
	#
	# Point
	x = point[0]
	z = point[1]
	# Source location
	x0 = srcloc[0]
	y0 = 0.0
	z0 = srcloc[1]
	# Source magnitude
	Mz = srcmag[2]
	# Parameter user
	Ly   = prm_user[0] # Length of the domain in the y direction
	nu   = prm_user[1] # Magnetic permeability
	ep   = prm_user[2] # Electric permeability
	sg   = prm_user[3] # Electric conductivity
	om   = prm_user[4] # Frequency
	beta = prm_user[5] # Fourier beta
	#
	Cn = 1.0*np.sqrt((2.0*np.pi*beta/Ly)**2+1j*nu*om*sg)
	Cn2= -(2.0*np.pi*beta/Ly)**2-1j*nu*om*sg
	Cn2= np.sqrt(-Cn2);
	R  = 1.0*np.sqrt((x-x0)**2+(z-z0)**2)
	K0=kv(0.0,Cn2*R)
	K1=kv(1.0,Cn2*R)
	K2=kv(2.0,Cn2*R);
	#
	if ((x==x0) and (z==z0)): case = 0 # in order to not have problems when point = srcloc
	#
	if case==0:
		H  = Mz/(4.0*np.pi*Ly)*(-2*1j*nu*sg*om*kv(0.0,Cn*R)-
								(2*Cn*kv(1.0,Cn*R))/R+
			 					(2*(z-z0)**2*Cn*kv(1.0,Cn*R))/R**3+
			 					((z-z0)**2*(4*np.pi**2*beta**2+1j*Ly**2*nu*sg*om)*
			 						(kv(0.0,Cn*R)+kv(2.0,Cn*R)))/
			 							(Ly**2*(x**2-2*x*x0+x0**2+(z-z0)**2))
			 					)
	if case==1:
		H  = Mz/2.0/np.pi*1.0/Ly*(-1j*nu*om*sg*K0+1.0/2.0*(K0+K2)*(Cn2*(z-z0)/R)**2
			-K1*Cn2*(R**2-(z-z0)**2)/R**3)*np.exp(1j*2.0*np.pi*beta*y0/Ly)
	#
	ReH = H.real
	ImH = H.imag
	#
	return ReH,ImH

# ============================================
# Build exact solution
def exact(point,srcloc,srcmag,prm_user,method):
	H_real = []
	H_imag = []
	for i in range(np.size(point.T[0])):
		aux1,aux2 = HZZ(point[i],srcloc[i],srcmag[i],prm_user,method)
		H_real.append(aux1)
		H_imag.append(aux2)
	#
	return np.asarray(H_real),np.asarray(H_imag)

# ============================================
# Read FE point value
def readfiles(namefile):
	#
	point  = []
	srcloc = []
	srcmag = []
	HzzRe  = []
	HzzIm  = []
	Hzzabs = []
	#
	f  = open(namefile)
	next(f) # Skip first line
	for fl,line in enumerate(f):
		point.append([float(line.split()[0]),float(line.split()[1])]);
		srcloc.append([float(line.split()[3]),float(line.split()[4])])
		srcmag.append([float(line.split()[6]),float(line.split()[7]),float(line.split()[8])])		
		HzzRe.append(float(line.split()[10]));
		HzzIm.append(float(line.split()[12]));
		Hzzabs.append(float(line.split()[14]));
	#
	return np.asarray(point),np.asarray(srcloc),np.asarray(srcmag),np.asarray(HzzRe),np.asarray(HzzIm),np.asarray(Hzzabs)
#######################################################################################
# Read Fortran data for homogenous 3D case
def readfiles2(namefile):
	#
	point  = []
	srcloc = []
	srcmag = []
	HzzRe  = []
	HzzIm  = []
	Hzzabs = []
	#
	f  = open(namefile)
	next(f) # Skip first line
	for fl,line in enumerate(f):
		point.append([float(line.split()[0]),float(line.split()[1])]);
		srcloc.append([float(line.split()[3]),float(line.split()[4])])
		srcmag.append([float(line.split()[6]),float(line.split()[7]),float(line.split()[8])])		
		HzzRe.append(float(line.split()[10]));
		HzzIm.append(float(line.split()[11]));
		Hzzabs.append(float(line.split()[12]));
	#
	return np.asarray(point),np.asarray(srcloc),np.asarray(srcmag),np.asarray(HzzRe),np.asarray(HzzIm),np.asarray(Hzzabs)

# ============================================
# Euclidean norm
def norm(anasol,numsol):
	#
	x2 = np.zeros(np.size(anasol))
	for i in range(np.size(anasol)):
		x2[i] = (anasol[i]-numsol[i])**2
	en = np.sqrt(np.sum(x2))
	mx = np.sqrt(np.max(x2))
	mn = np.sqrt(np.min(x2))
	#
	return en,mx,mn

# ========================================================================#
#                                 Main Code                               #

########################### - Read from terminal - ########################

# Read data
d       = int(sys.argv[1]) # dimension
Nkn     = int(sys.argv[2]) # Number on knots
p       = int(sys.argv[3]) # polynomial degree
#
nlevel  = int(sys.argv[4]) # number of patition levels (use 0 for IGA) e.g., 0,1,2,...
nbetas  = int(sys.argv[5]) # Fourier discretization of the y direction (number of betas) e.g., 0,1,2,...
npnts   = int(sys.argv[6]) # Number of receivers (per subdomain) e.g., 1,2,3,...
sg      = int(sys.argv[7]) # Number of subdomains e.g., 1,2,3,...
#
srcid   = int(sys.argv[8]) # Source case e.g., 1,2,3,...
# 
#
mu      = convert2num(str(sys.argv[9]))  # mu
epsilon = convert2num(str(sys.argv[10])) # epsilon
sigma   = convert2num(str(sys.argv[11])) # sigma_
omega   = convert2num(str(sys.argv[12])) # omega
# 
# 
printid = int(sys.argv[13]) # 0 nothing plotted, 1 creates plots
sub 	= int(sys.argv[14]) # subtrajectory


######################## - Set additional param - #########################

prm_user = np.zeros(6)
prm_user[1] = mu         # mu
prm_user[2] = epsilon    # epsilon
prm_user[3] = sigma      # sigma
prm_user[4] = omega      # omega
prm_user[5] = nbetas     # Beta

########################### - Read from file - ############################

HzzRe_ana = 0.0
HzzIm_ana = 0.0
HzzRe_num = 0.0
HzzIm_num = 0.0

dirf  = "Hzz/"
namef = "Hzz_"
extf  = ".txt"
for i in range(0,nbetas+1):
	middlef_r  = "N"+str(Nkn)+"_p"+str(p+1)+"_l"+str(nlevel)+"_bt"+str(i)+"_sg"+str(sg)+"_Pos"+str(npnts)+"_Src"+str(srcid)+"_sigma"+str(sigma)
	filename = dirf+namef+middlef_r+extf 
	point_i,srcloc_i,srcmag_i,HzzRe_i,HzzIm_i,Hzzabs_i = readfiles(filename)
	#
	prm_user[5] = i # Beta
	#
	if (i==0): c = 1.0
	if (i> 0): c = 2.0
	HzzRe_num=HzzRe_num+c*HzzRe_i
	HzzIm_num=HzzIm_num+c*HzzIm_i

Hzz_ana = np.sqrt(HzzRe_ana**2+HzzIm_ana**2)
Hzz_num = np.sqrt(HzzRe_num**2+HzzIm_num**2)


################## - Write total Hzz values in a file - ##################

dirf      = "Hzz_Full/"
namef     = "Full_Hzz_"
middlef_w = "N"+str(Nkn)+"_p"+str(p+1)+"_l"+str(nlevel)+"_Pos"+str(npnts)+"_Src"+str(srcid)+"_sigma"+str(sigma)+"_sub"+str(sub)
extf     = ".f90"
filename = dirf+namef+middlef_w+extf 

if sg == 1:  
	f = open(filename,"w+")
	f.write("Segment|src_zloc|Rec1_zloc |     Rec1_Hzz_Re   |     Rec1_Hzz_Im    |Rec2_zloc |    Rec2_Hzz_Re    |     Rec2_Hzz_Im\n")
	f.close()

f = open(filename,"a")
f.write("  %d    | %1.4f | %1.6f | %1.16f| %1.16f| %1.6f | %1.16f| %1.16f\n" % (sg,srcloc_i[1][1],point_i.T[1][0],HzzRe_num[0],HzzIm_num[0],point_i.T[1][1],HzzRe_num[1],HzzIm_num[1]))

f.close()

################## - Write Attenuation and phase in a file - ##################

Hzz_num     = HzzRe_num+1j*HzzIm_num
Hzz_num_log = np.log(Hzz_num[0]/Hzz_num[1])
Azz_num     = Hzz_num_log.real
Pzz_num     = Hzz_num_log.imag


dirf      = "APzz/"
namef     = "APzz_"
middlef_w = "N"+str(Nkn)+"_p"+str(p+1)+"_l"+str(nlevel)+"_Pos"+str(npnts)+"_Src"+str(srcid)+"_sigma"+str(sigma)+"_sub"+str(sub)
extf     = ".txt"
filename = dirf+namef+middlef_w+extf 

if sg == 1:  
	f = open(filename,"w+")
	f.write("Segment| src_xloc|src_zloc|        Azz                Pzz\n")
	f.close()

f = open(filename,"a")
f.write("  %d    | %1.4f | %1.4f | %1.16f  %1.16f\n" % (sg,srcloc_i[1][0],srcloc_i[1][1],Azz_num,Pzz_num))

f.close()

