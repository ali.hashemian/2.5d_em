import sys
import numpy       as np
from   igakit.cad  import *
from   igakit.io   import *
from   igakit.plot import plt
from   colorama    import Fore, Back, Style 

def isfloat(value):
  try:
    float(value)
    return True
  except ValueError:
    return False

def convert2num(str):
  aux  = 1.0
  sstr = np.size(str.split("*"))
  for i in range(sstr):
    line = str.split("*")[i]
    if isfloat(line):
        aux = aux*float(line)
    else: 
        aux = aux*ispi(line)
  return aux
# 
# 
# Knot generator
def generate_knots_power(N,sn,h,p):
    #
    # sn = 64   # Number of elements in the interior of the mesh (interior mesh of sn*sn elements)
                # Note that the remaining elements are used to create the gradient of element size.
                # For now this is dirty, we can improve it by introduce sn by command terminal or 
                # do it automatically.
    #
    hm = 0.0
    #
    knots_in = []
    for i in range(int(0.5*sn)+1):
        hm = h*(i)
        knots_in.append(hm)
    #
    knots_ext = []
    for i in range(int(0.5*(N-sn))+1):
        if i==0:
            hm = hm + h*np.log(i+1)
        else:
            hm = hm + h*2.1**(i) # 1.125 for n128 sn64 h0.02 # 2.1 for n64 sn50 h0.025
        knots_ext.append(hm)
    #
    knots_rht = knots_in[:-1]+knots_ext
    #
    knots_lft = knots_rht[::-1]
    knots_lft = [-1.0*x for x in knots_lft]
    knots = knots_lft[:-1]+knots_rht
    #
    U = [min(knots)]*p + knots + [max(knots)]*p
    #
    return np.asarray(U, dtype='d')    


# Grid generator
def generate_grid(x0,i,Nkn,p,base, breakx=(),breaky=()):
    assert base[0].dim == 1
    assert base[1].dim == 1
    base_x = base[0].copy()
    base_y = base[1].copy()
    px = base_x.degree[0]
    py = base_y.degree[0]
    for u in breakx:
        base_x.insert(0, u, p-i)
    for u in breaky:
        base_y.insert(0, u, p-i)
    Ux = base_x.knots[0]+x0[0]#/(1.0*Nkn)*2.0*lg+x0[0]
    Uy = base_y.knots[0]+x0[1]#/(1.0*Nkn)*2.0*lg+x0[1]
    #
    grid = NURBS([Ux,Uy])
    return grid

# Print data
def print_grid(grid):
    plt.figure()
    plt.cplot(grid)
    plt.kplot(grid)
    #plt.plot(grid)
    #plt.kwire(grid, color=(0,0,0))
    #plt.kpoint(grid, color=(0,0,1))
    #plt.surface(grid, color=(0.7,0.7,0.7))
    plt.show()

# Finding the center of the mesh
def mesh_center(h,sn,theta,sgstep,xsrc,zsrc,nsrc):
    # The objective is to find the mesh center [xm,ym] so that we put the source(s) at the center of respective element(s)
    if nsrc == 1: # we have one source
        th  =  theta*np.pi/180.0
        R   =  np.array([[np.cos(th), -np.sin(th)],
                         [np.sin(th),  np.cos(th)]])
        # 
        L0  =  np.array([0, sn/4]) 
        Lp  =  R.dot(L0)
        m   =  np.ceil(np.abs(Lp[0]))  # Nr. of elements between source and mesh center in vertical dir. (integer)
        k   =  np.ceil(np.abs(Lp[1]))  # Nr. of elements between source and mesh center in horizontal dir. (integer)
        xm  = -(h*m+h/2.0) + xsrc 
        ym  =   h*k+h/2.0  + zsrc 
        # 
        return xm, ym
    #
    else:  # nsrc = 2: # we have two sources and put them symmetrically, the tool center and mesh center are almost the same
        TL =  1.13665; # tool length in   S2----R2----R1----S1->
        th =  theta*np.pi/180.0
        M  = TL*np.sin(th)/h
        K  = TL*np.cos(th)/h
        m  = np.ceil((M+1)/2)
        k  = np.ceil((K+1)/2)
        hm = h
        hk = h
        xm = -m*hm+hm/2 + xsrc 
        ym =  k*hk-hk/2 + zsrc 
        # 
        return xm, ym, hm, hk

# ============================================
# ============================================
## Main
#- Read data.
d      = int(sys.argv[1])               # dimension
Nkn    = int(sys.argv[2])               # Number on knots
p      = int(sys.argv[3])               # polynomial degree
nlevel = int(sys.argv[4])               # number of patition levels (use 0 for IGA) e.g., 0,1,2,3,...
sg     = int(sys.argv[5])               # segment number e.g., 1,2,3,...
theta  = convert2num(str(sys.argv[7]))  # the tool orientattion (in degree) wrt vertical dir.
sgstep = convert2num(str(sys.argv[8]))  # moving steps of the tool (m) in different segments
xsrc   = convert2num(str(sys.argv[9]))  # x ccordinate (m) of the src at the begining of the subtrajectory
zsrc   = convert2num(str(sys.argv[10])) # z ccordinate (m) of the src at the begining of the subtrajectory
caseno = convert2num(str(sys.argv[11])) # case number in random sampling for training test


sn = 50
h  = 0.025 # Elemental size

xm, ym, hx, hy = mesh_center(h,sn,theta,sgstep,xsrc,zsrc,2)
#- Data used to build the domain
#- Building domain
Gx  = generate_knots_power(Nkn,sn,hx,p)
Gy  = generate_knots_power(Nkn,sn,hy,p)
Gpx = generate_knots_power(Nkn,sn,hx,p+1)
Gpy = generate_knots_power(Nkn,sn,hy,p+1)
Lx  = generate_knots_power(Nkn,sn,hx,1)
Ly  = generate_knots_power(Nkn,sn,hy,1)
# Curl X H1
base_u = [NURBS([Gx]) ,NURBS([Gpy])]
base_v = [NURBS([Gpx]),NURBS([Gpy])]
base_w = [NURBS([Gpx]),NURBS([Gy])]
base_g = [NURBS([Lx]) ,NURBS([Ly])]
base_l = [NURBS([Lx]) ,NURBS([Ly])]
# 
for level in range(nlevel+1):
    step = Nkn//2**level
    if step < 1: continue
    breaks_fx = Gx[p:-p:step][1:-1]
    breaks_fy = Gy[p:-p:step][1:-1]
    breaks_gx = Lx[p:-p:step][1:-1]
    breaks_gy = Ly[p:-p:step][1:-1]
    nrb_u = generate_grid([xm,ym],1,Nkn,p,base_u,breaks_fx,breaks_fy)
    nrb_v = generate_grid([xm,ym],1,Nkn,p,base_v,breaks_fx,breaks_fy)
    nrb_w = generate_grid([xm,ym],1,Nkn,p,base_w,breaks_fx,breaks_fy)
    nrb_g = generate_grid([xm,ym],1,Nkn,1,base_g,breaks_gx,breaks_gy)
    nrb_l = generate_grid([xm,ym],1,Nkn,1,base_l,breaks_gx,breaks_gy)
    #
    if int(sys.argv[6])==1:
        print "Geometry No.%i"%(level)
        print "grid-x No.%i"%(level)
        print_grid(nrb_u)
        print "Grid_y No.%i"%(level)
        print_grid(nrb_v)
        print "Grid_z No.%i"%(level)
        print_grid(nrb_w)
        print "Grid_g No.%i"%(level)
        print_grid(nrb_g)
        print "Grid_L No.%i"%(level)
        print_grid(nrb_l)
    #
    basename = "geometry/case"+str(caseno)+"-grid_n" + str(Nkn) + "-p" + str(p) + "-c" + str(p-1) + "-l" + str(level) + "-sg" + str(sg) 
    PetIGA(scalar='complex').write(basename + "-Hx.dat",  nrb_u)
    PetIGA(scalar='complex').write(basename + "-Hy.dat",  nrb_v)
    PetIGA(scalar='complex').write(basename + "-Hz.dat",  nrb_w)
    PetIGA(scalar='complex').write(basename + "-geo.dat", nrb_g)
    PetIGA(scalar='complex').write(basename + "-geo_lin.dat", nrb_l)
    #
