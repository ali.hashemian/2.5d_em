#!/bin/bash

# Other commands must follow all #SBATCH directives...
source activate python2.7_env
export OMP_NUM_THREADS=1 
# =========================================== #
# Computing a 2D EM problem
# =========================================== #
## Discretization
# Dimension
d=2
# Number of elements in each direction
lkn=(64)
# Polynomial order (in H-curl, we have P,P-1)
P=(3)
# Number of betas
Nbt=(70)
# tool orientattion (in degree) wrt vertical direction, at each sub-trajectory
theta=$4
# horizontal length (m) of each subtrajectory for a moving tool
X=(15) 
# Number of subgrids at each sub-trajectory (X, Theta, and Nsg should be on the same size)
Nsg=(1)
# 
PI=$(echo "4*a(1)" | bc -l)
# ============================================= #
# Parameters                                    #
# ============================================= #
# mu
mu=4.0*pi*1E-7
# epsilon
epsilon=8.85e-12
# sigma
sigma=$1
sigma_u=$2
sigma_l=$3
afactor=1.0 # far cases that we have different sigmas in vertical and horizontal directions
du=$5
dl=$6
TL=1.13665
# frequency (in kHz)
freq=2000
omega=2.0*pi*1000*$freq
# the case number in random sampling for training test
caseno=$7
# 
# ============================================= #
# ============================================= #
# Main code                                     #
# ============================================= #
  for kn in "${lkn[@]}"; do
    for pp in "${P[@]}"; do
      for nbt in "${Nbt[@]}"; do  # This line is added to see the effect of different No. of betas on results
        ## Nested dissections levels refined
        #  NDLr is the number of levels in each spatial dimension
        p=$(echo "$pp-1" | bc)
        N=$kn^$d
        ND=$(echo "($kn+$p)^($d)" | bc)
        NDL=$(echo $ND | awk '{printf "%.i\n", $0}')
        NDLT=$(echo 'l('$NDL')/l(2)/'$d'' | bc -l)
        NDLr=${NDLT%.*}
        gp=$(echo "($p+1)" | bc)
        gp2=$(echo "($p+2)" | bc)
        k=$(echo "($p-1)" | bc)
        btp=$(echo "($nbt-1)" | bc)
        #
        # ## Solver
        echo "--- Solving case ${caseno%.*}"
        # rIGA
        for l in $(seq 3 3 3); do # $NDLr); do
          xs=0; zs=0; sgi=0; # starting values
          for i in $(eval echo {0..$(echo ${#X[@]}-1 |bc)}); do # i is subtrajectory index
            x=$(echo "${X[$i]}" | bc); # echo "x = $x"
            nsg=$(echo "${Nsg[$i]}" | bc); # echo "nsg = $nsg"
            th=$(echo "$theta*$PI/180" | bc -l); # echo "theta = $th"
            step=$(echo "$x/$nsg" | bc -l); # echo "----- step = $step"
            for sg in $(seq 1 1 $nsg); do
              xsrc=$(echo "$xs+($sg-1)*$step" | bc -l); # echo "xsrc = $xsrc"
              zsrc=$(echo "$zs-($sg-1)*$step*c($th)/s($th)" | bc -l); # echo "xsrc, zsrc = [$xsrc, $zsrc]"              
              # echo "  - Create mesh files"
              python 2.5D_EM_meshgen.py $d $kn $p $l $sg 0 $theta $step $xsrc $zsrc $caseno
              # echo " "
              # echo "  - Solve the problem"
              ./2.5D_EM -theta $theta -step $step -xsrc $xsrc -zsrc $zsrc -nel $kn -l $l -sg $sg -nbetas $nbt -mu $mu -ep $epsilon -om $omega -sm $sigma -smu $sigma_u -sml $sigma_l -af $afactor -du $du -dl $dl -cs $caseno -TL $TL -Hx_iga_quadrature $gp2,$gp2 -Hy_iga_quadrature $gp2,$gp2 -Hz_iga_quadrature $gp2,$gp2 -Hx_iga_load geometry/case$caseno-grid_n$kn-p$p-c$k-l$l-sg$sg-Hx.dat -Hy_iga_load geometry/case$caseno-grid_n$kn-p$p-c$k-l$l-sg$sg-Hy.dat -Hz_iga_load geometry/case$caseno-grid_n$kn-p$p-c$k-l$l-sg$sg-Hz.dat -geo_iga_quadrature $gp2,$gp2 -geo_iga_load geometry/case$caseno-grid_n$kn-p$p-c$k-l$l-sg$sg-geo.dat -geo_lin_iga_load geometry/case$caseno-grid_n$kn-p$p-c$k-l$l-sg$sg-geo_lin.dat # -log_view | tee log/2.5DEM_N$kn\_p$pp\_l$l\_case$caseno.f90
              # echo " "
              # echo " "
              python 2.5D_EM_sol.py $d $kn $p $l $btp 1 $sg 1 $mu $epsilon $caseno $omega 0 $i
              python 2.5D_EM_sol.py $d $kn $p $l $btp 1 $sg 2 $mu $epsilon $caseno $omega 0 $i
              python 2.5D_EM_sol_2_source.py $d $kn $p $l $btp 1 $caseno $i
            done
            sgi=$(echo "$sgi+$nsg" | bc)
            xs=$(echo "$xs+$x" | bc -l)
            zs=$(echo "$zs-$x*c($th)/s($th)" | bc -l)
          done
        done
      done
    done
  done

# ============================================= #