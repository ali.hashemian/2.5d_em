The computer codes contributed to the research results reported in the paper as well as the varying 
parameters and generated synthetic dataset are publicly available on
https://gitlab.com/ali.hashemian/2.5d em.


--------- 2.5D_EM_parallelrun.py ---------
------------------------------------------
The file 2.5D_EM_parallelrun.py executes the parallel computations. 
We write this code in a Python 2.7 environment, which is based on Anaconda (available on 
https://repo.anaconda.com/archive). Note: after installing Anaconda, the Python 2.7 environment should be
activated.
In the 2.5D_EM_parallelrun.py file, we set different parameters for our model problems and export them as
inputs to the run-riga-em.sh file. 
All the input variables are fully described in the file. For a parallel run, the total number of samples
and total number of CPU/threads can be selected inside the Python file.


------------- run-riga-em.sh -------------
------------------------------------------
This file is called automatically inside the 2.5D_EM_parallelrun.py.
This shell file consists of three parts. 
1) The preprocessing part that generates the computational mesh using 2.5D_EM_meshgen.py. Therein we 
employ igakit (available on https://bitbucket.org/dalcinl/igakit) to create appropriate B-spline 
discretizations of the computational domain. 
2) The main part is 2.5D_EM.c code that solves the 2.5D borehole EM problem. The code needs PETSc 
(available on https://gitlab.com/petsc/petsc) and PetIGA (available on https://bitbucket.org/dalcinl/PetIGA) 
packages to be executed. It is important to note that PETSc should be configured with complex scalars, 
METIS, OpenMPI, ScaLAPACK, and Intel MKL libraries to have a computationally efficient solution. The 
latter library contains PARDISO as our sparse direct solver. Note: Intel MKL should be installed a priori 
(available on https://software.intel.com/content/www/us/en/develop/tools/math-kernel-library).
3) Finally, the third part of the shell file performs the postprocessing in Python using 2.5D_EM_sol.py 
and 2.5D_EM_sol_2_source.py files. These files calculate apparent resistivities based on attenuation and 
phase. Note: if only one magnetic source is considered, we do not need 2.5D_EM_sol_2_source.py file.


----------- 2.5D_EM_meshgen.py -----------
------------------------------------------
This file is used to generates the computational mesh and called automatically inside the run-riga-em.sh 
bash.
The input parameters of this file are already set inside 2.5D_EM_parallelrun.py and run-riga-em.sh based 
on the model problems.
We write this code in a Python 2.7 environment. Note: igakit (available on 
https://bitbucket.org/dalcinl/igakit) should be installed a priori.
A practical tutorial for igakit is available on https://petiga-igakit.readthedocs.io/en/latest.


--------------- 2.5D_EM.c ----------------
------------------------------------------
This file solves the 2.5D borehole EM problem and is called automatically inside the run-riga-em.sh bash.
The input parameters of this file are already set inside 2.5D_EM_parallelrun.py and run-riga-em.sh based 
on the model problems.
The code needs PETSc (available on https://gitlab.com/petsc/petsc) and PetIGA (available on 
https://bitbucket.org/dalcinl/PetIGA) packages to be executed.
A comprehensive documentation regarding PETSc can be found on https://www.mcs.anl.gov/petsc.
A practical tutorial for PetIGA is available on https://petiga-igakit.readthedocs.io/en/latest.
Note: before the first implementation, we need to compile this C code using make 2.5D_EM.c.


--- 2.5D_EM_sol.py and 2.5D_EM_sol_2_source.py ---
--------------------------------------------------
These files perform the postprocessing and are called automatically inside the run-riga-em.sh bash.
The input parameters of these files are already set inside 2.5D_EM_parallelrun.py and run-riga-em.sh 
based on the model problems.
We write these codes in a Python 2.7 environment.
These codes are almost the same. They calculate the attenuation rations and phase differences useful for 
obtaining apparent resistivities. Note: if only one magnetic source is considered, we do not need 
2.5D_EM_sol_2_source.py file.


------------ VaryingParameters.txt ---------------
--------------------------------------------------
These file contains all varying parameters thats are used for dataset generation. The variables are 
selected randomly based on the variable ranges reported in Table 2 of the article.


---------- ApparentResistivities.txt -------------
--------------------------------------------------
Apparent resistivities based on attenuation ratio (rho_A) and phase difference (rho_P) obtained from 
varying parameters of Table 2.