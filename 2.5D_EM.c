#include <unistd.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <stddef.h>
#include <ctype.h>
#include <stdbool.h>
#include <stdlib.h>
#include "petiga.h"
#include "petigam.h"
#include "../src/petigaftn.h"

//----------------------------------------------------------------------------------------------------------------------------//
// User parameters
typedef struct {
  // Source parameters
  PetscReal   alpha;
  // Source data
  PetscInt    sr_id;
  PetscReal   *srcloc[3];
  PetscReal   *srcmag[3];
  PetscInt    *srcelem;
  PetscReal   r0;
  PetscInt    *srczone;
  // Properties (Improve this part)
  PetscReal   mu;
  PetscReal   sigma;
  PetscReal   sigma_u;
  PetscReal   sigma_l;
  PetscReal   afactor;
  PetscReal   omega;
  PetscScalar sigma_bar[2][3];
  PetscScalar mu_bar;
  // Fourier discretization
  PetscInt    nbetas;
  PetscInt    beta;
  PetscReal   Ly;
  PetscInt    sg;
  // Solver type
  char solver_type[8];
  // 
  PetscReal theta;
  PetscReal step;
  PetscReal xsrc;
  PetscReal zsrc;
  // Database generation for DL inversion
  PetscReal caseno; 
  PetscReal du;
  PetscReal dl;
  PetscReal TL;
} Params;

//----------------------------------------------------------------------------------------------------------------------------//
// Pi number
#define M_PI 3.14159265358979323846

//----------------------------------------------------------------------------------------------------------------------------//
// check if float
bool is_float(const char *s, PetscReal *dest) {
  if (s == NULL) {
    return PETSC_FALSE;
  }
  char *endptr;
  *dest = (PetscReal) strtod(s, &endptr);
  if (s == endptr) {
    return PETSC_FALSE; // no conversion;
  }
  // Look at trailing text
  while (isspace((unsigned char ) *endptr))
    endptr++;
  return *endptr == '\0';
}

//----------------------------------------------------------------------------------------------------------------------------//
// check if pi
void is_pi(const char *s, PetscReal *dest) {
  const char *s2 = "pi";
  PetscInt aux;
  if (s != NULL){
    aux = strcmp(s,s2);
    if (aux==0){*dest = PETSC_PI;}
  }
}

//----------------------------------------------------------------------------------------------------------------------------//
// Converting
void cv_str2num(char string[],PetscReal *value)
{
  const char delimiters[] = "*";
  char * running = strdup(string);
  char * token;
  PetscReal aux=1.0;
  *value = 1.0;
  while( token != NULL ) { 
    token = strsep(&running, delimiters); 
    if (token == NULL){break;}
    is_float(token,&aux);
    is_pi(token,&aux);
    *value = *value * aux;
  }
}

//----------------------------------------------------------------------------------------------------------------------------//
// Rotation Matrix
#undef  __FUNCT__
#define __FUNCT__ "rot_mat"
PetscErrorCode rot_mat(PetscReal vec[], PetscReal theta)
{
  PetscReal vold[3];
  vold[0] = vec[0];
  vold[2] = vec[2];

  vec[0]  = vold[0]*cos(theta*PETSC_PI/180)-vold[2]*sin(theta*PETSC_PI/180);
  vec[2]  = vold[0]*sin(theta*PETSC_PI/180)+vold[2]*cos(theta*PETSC_PI/180); 
  PetscFunctionReturn(0);
}

//----------------------------------------------------------------------------------------------------------------------------//
// Set values
#undef  __FUNCT__
#define __FUNCT__ "set_values"
PetscErrorCode set_values(PetscReal data[], PetscReal **src, PetscInt id)
{
  src[0][id] = data[0];
  src[1][id] = data[1];
  src[2][id] = data[2];
  PetscFunctionReturn(0);
}

//----------------------------------------------------------------------------------------------------------------------------//
// Sigma property 
// This subroutine allow to set the values of sigma into the domain
void fsigma_loc(const PetscReal xyz[],PetscInt *loc,void *ctx)
{
  Params *user  = (Params*)ctx;
  PetscReal x  = xyz[0];
  PetscReal z  = xyz[1];
  // This is used to set the property sigma (Identify in what zone we are when we are assembling the system)  
  PetscInt Case = 5;
  // 
  if      (Case == 0) {*loc=0;} // Homogeneous media
  else if (Case == 1) { // all these vases are heterogeneous media
    if      (z >= -0.8)                             {*loc=0;}
    else if (z >= -1.2   && z < -0.8   && x <= 7.5) {*loc=1;}
    else if (z >= -1.2   && z < -0.8   && x >  7.5) {*loc=0;}
    else if (z >= -1.445 && z < -1.2   && x <= 7.5) {*loc=2;}
    else if (z >= -1.445 && z < -1.2   && x >  7.5) {*loc=0;}
    else if (z >= -1.845 && z < -1.445 && x <= 7.5) {*loc=2;}
    else if (z >= -1.845 && z < -1.445 && x >  7.5) {*loc=1;}
    else if (z <  -1.845)                           {*loc=2;}
  } 
  else if (Case == 2) { // Heterogeneous media, one geological fault
    if      (z >= -0.5)                             {*loc=0;}
    else if (z >= -1.0   && z < -0.5   && x <= 7.5) {*loc=1;}
    else if (z >= -1.0   && z < -0.5   && x >  7.5) {*loc=0;}
    else if (z >= -1.500 && z < -1.0   && x <= 7.5) {*loc=2;}
    else if (z >= -1.500 && z < -1.0   && x >  7.5) {*loc=0;}
    else if (z >= -2.000 && z < -1.500 && x <= 7.5) {*loc=2;}
    else if (z >= -2.000 && z < -1.500 && x >  7.5) {*loc=1;}
    else if (z <  -2.000)                           {*loc=2;}
  }
  else if (Case == 3) { // Heterogeneous media, not in the paper
    PetscInt phi = 85;
    PetscReal x0 = 0;   PetscReal z0 = -1;
    PetscReal x1 = 30;  PetscReal z1 = -7;
    PetscReal x2 = 60;  PetscReal z2 = -10;  PetscReal z3 = -14;  PetscReal z4 = -16;
    PetscReal x3 = 90;  PetscReal z5 = -17;  PetscReal z6 = -23;
    if (x <= x1) {
      if      (z+x/tan(phi*PETSC_PI/180) >  z0+x0/tan(phi*PETSC_PI/180))            {*loc=0;}
      else if (z+x/tan(phi*PETSC_PI/180) <= z0+x0/tan(phi*PETSC_PI/180) && z >  z1) {*loc=1;}
      else if (z+x/tan(phi*PETSC_PI/180) >  z1+x0/tan(phi*PETSC_PI/180) && z <= z1) {*loc=2;}
      else if (z+x/tan(phi*PETSC_PI/180) <= z1+x0/tan(phi*PETSC_PI/180))            {*loc=0;}}
    else if (x > x1 && x <= x2) {
      if      (z >  z2)             {*loc=0;}
      else if (z <= z2 && z >  z3)  {*loc=1;}
      else if (z <= z3 && z >  z4)  {*loc=2;}
      else if (z <= z4)             {*loc=0;}}
    else if (x > x2 && x <= x3) {
      if      (z+x/tan(phi*PETSC_PI/180) >  z5+x2/tan(phi*PETSC_PI/180))            {*loc=0;}
      else if (z+x/tan(phi*PETSC_PI/180) <= z5+x2/tan(phi*PETSC_PI/180) && z >  z6) {*loc=1;}
      else if (z+x/tan(phi*PETSC_PI/180) >  z6+x2/tan(phi*PETSC_PI/180) && z <= z6) {*loc=2;}
      else if (z+x/tan(phi*PETSC_PI/180) <= z6+x2/tan(phi*PETSC_PI/180))            {*loc=0;}}
  }
  else if (Case == 4) { // Heterogeneous media, two geological faults, inclined layers
    PetscInt phi = 95;
    PetscReal x0 = 0;   PetscReal z0 = -3;
    PetscReal x1 = 30;  PetscReal z1 = -4;  PetscReal z11 = -8;   
    PetscReal x2 = 60;  PetscReal z2 = -6;  PetscReal z3  = -9;  PetscReal z4 = -11;
                        PetscReal z5 = -0;  PetscReal z6  = -3;  PetscReal z7 = -5; 
    if (x <= x1) {
      if      (z+x/tan(phi*PETSC_PI/180) >   z0+x0/tan(phi*PETSC_PI/180))            {*loc=0;}
      else if (z+x/tan(phi*PETSC_PI/180) <=  z0+x0/tan(phi*PETSC_PI/180) && z >  z1) {*loc=1;}
      else if (z+x/tan(phi*PETSC_PI/180) >  z11+x0/tan(phi*PETSC_PI/180) && z <= z1) {*loc=2;}
      else if (z+x/tan(phi*PETSC_PI/180) <= z11+x0/tan(phi*PETSC_PI/180))            {*loc=0;}}
    else if (x > x1 && x <= x2) {
      if      (z >  z2)             {*loc=0;}
      else if (z <= z2 && z >  z3)  {*loc=1;}
      else if (z <= z3 && z >  z4)  {*loc=2;}
      else if (z <= z4)             {*loc=0;}}
    else if (x > x2) {
      if      (z+x/tan(phi*PETSC_PI/180) >  z5+x2/tan(phi*PETSC_PI/180))            {*loc=0;}
      else if (z+x/tan(phi*PETSC_PI/180) <= z5+x2/tan(phi*PETSC_PI/180) && z >  z6 
            && z+x/tan(phi*PETSC_PI/180) >  z7+x2/tan(phi*PETSC_PI/180))            {*loc=1;}
      else if (z+x/tan(phi*PETSC_PI/180) >  z7+x2/tan(phi*PETSC_PI/180) && z <= z6) {*loc=2;}
      else if (z+x/tan(phi*PETSC_PI/180) <= z7+x2/tan(phi*PETSC_PI/180))            {*loc=0;}}
  }
  else if (Case == 5) { // Database generation for DL inversion
    PetscReal th = user->theta;
    PetscReal TL = user->TL;
    PetscReal du = user->du;
    PetscReal dl = user->dl;
    PetscReal hu = du + TL/2*cos(th*PETSC_PI/180);   
    PetscReal hl = dl - TL/2*cos(th*PETSC_PI/180); 
    if      (z >   hu)              {*loc=1;} // sigma_u
    else if (z <=  hu && z >= -hl)  {*loc=0;} // sigma_c
    else if (z <  -hl)              {*loc=2;} // sigma_l
  }
}

//----------------------------------------------------------------------------------------------------------------------------//
// Forcing (Source value)
// This subroutine set the source
void Forcing(const PetscReal xyz[],PetscScalar forcing[],void *ctx)
{
  Params *user  = (Params*)ctx;
  // Coordinates of the source
  PetscReal x0 = user->srcloc[0][user->sr_id];
  PetscReal y0 = user->srcloc[1][user->sr_id];
  PetscReal z0 = user->srcloc[2][user->sr_id];
  // Sourcing case
  PetscInt Case = 2;
  if (Case == 1) { // Elemental source value
    forcing[0] = user->srcmag[0][user->sr_id]*exp(-2*PETSC_PI*PETSC_i*user->beta*y0/user->Ly)/user->Ly;
    forcing[1] = user->srcmag[1][user->sr_id]*exp(-2*PETSC_PI*PETSC_i*user->beta*y0/user->Ly)/user->Ly;
    forcing[2] = user->srcmag[2][user->sr_id]*exp(-2*PETSC_PI*PETSC_i*user->beta*y0/user->Ly)/user->Ly;
  }
  else if (Case == 2) { // Domain source value
    // Point coordinates
    PetscReal x  = xyz[0];
    PetscReal z  = xyz[1];
    forcing[0] = user->srcmag[0][user->sr_id]*exp(-(user->alpha)*(pow(x-x0,2)+pow(z-z0,2)))*exp(-2*PETSC_PI*PETSC_i*user->beta*y0/user->Ly)/user->Ly;
    forcing[1] = user->srcmag[1][user->sr_id]*exp(-(user->alpha)*(pow(x-x0,2)+pow(z-z0,2)))*exp(-2*PETSC_PI*PETSC_i*user->beta*y0/user->Ly)/user->Ly;
    forcing[2] = user->srcmag[2][user->sr_id]*exp(-(user->alpha)*(pow(x-x0,2)+pow(z-z0,2)))*exp(-2*PETSC_PI*PETSC_i*user->beta*y0/user->Ly)/user->Ly;
  }
}

//----------------------------------------------------------------------------------------------------------------------------//
// Identify the element that contain the source
#undef  __FUNCT__
#define __FUNCT__ "Get_ele_src"
PetscErrorCode Get_ele_src(IGA iga, PetscInt n_sr, void *ctx)
{
  PetscErrorCode ierr;
  Params *user  = (Params*)ctx;
  PetscInt i;
  IGAElement     element;

  PetscFunctionBegin;
  PetscValidHeaderSpecific(iga,IGA_CLASSID,1);
  IGACheckSetUp(iga,1);

  /* Element loop */
  ierr = IGABeginElement(iga,&element);CHKERRQ(ierr);
  while (IGANextElement(iga,element)) {
    // Simple way to check if the point is into the element
    if (!element->geometry) SETERRQ1(PETSC_COMM_WORLD,1,"No geometry data. Geometry=%s\n",element->geometry ? "true" : "false");
      // loop of sources
    for (i=0;i<n_sr;i++)
    {
      // point data
      PetscReal     x = user->srcloc[0][i];
      PetscReal     z = user->srcloc[2][i];
      // geometry data
      PetscReal *X = element->geometryX;
      // auxiliar parameter
      PetscInt in=0;
      // indices of first and last node of the element
      PetscInt a = 0;
      PetscInt b = element->nen*element->nsd-element->nsd;
      // check operation
      if ( x <= X[a  ] ) in += 1;
      if ( z <= X[a+1] ) in += 1;
      if ( x >= X[b  ] ) in += 1;
      if ( z >= X[b+1] ) in += 1;
      // set Element id
      if ( in == 0 ) user->srcelem[i] = element->index;
    }
  }
  ierr = IGAEndElement(iga,&element);CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

//----------------------------------------------------------------------------------------------------------------------------//
// Identify the zone that contain the source
#undef  __FUNCT__
#define __FUNCT__ "Get_zone_src"
PetscErrorCode Get_zone_src(IGA iga, PetscInt n_sr, void *ctx)
{
  PetscErrorCode ierr;
  Params *user  = (Params*)ctx;
  PetscInt i;
  IGAElement     element;

  PetscFunctionBegin;
  PetscValidHeaderSpecific(iga,IGA_CLASSID,1);
  IGACheckSetUp(iga,1);

  /* Element loop */
  ierr = IGABeginElement(iga,&element);CHKERRQ(ierr);
  while (IGANextElement(iga,element)) {
    // Simple way to check if the point is into the element
    if (!element->geometry) SETERRQ1(PETSC_COMM_WORLD,1,"No geometry data. Geometry=%s\n",element->geometry ? "true" : "false");
      // loop of sources
    for (i=0;i<n_sr;i++)
    {
      // point data
      PetscReal     x = user->srcloc[0][i];
      PetscReal     z = user->srcloc[2][i];
      // geometry data
      PetscReal *X = element->geometryX;
      // indices of first and last node of the element
      PetscInt a = 0;
      PetscInt b = element->nen*element->nsd-element->nsd;
      // center of the element
      PetscReal mid[2];
      mid[0] = 0.5*(X[a  ] + X[b  ]);
      mid[1] = 0.5*(X[a+1] + X[b+1]);
      // distant from the center of the element to the source point
      PetscReal r;
      r = sqrt(pow((x-mid[0]),2)+pow((z-mid[1]),2));
      // r0
      PetscReal r0 = user->r0;
      // set element id
      if (r <= r0) {user->srczone[element->index] = i; break;}
      else {user->srczone[element->index] = -1;}
    }
  }
  ierr = IGAEndElement(iga,&element);CHKERRQ(ierr);

  PetscFunctionReturn(0);
}


//----------------------------------------------------------------------------------------------------------------------------//
//----------------------------------------------------------------------------------------------------------------------------//
//----------------------------------------------------------------------------------------------------------------------------//
// This segment was developed to Build the matrix in parts in order to allow the reuse of the matrix to reduce the cost of
// assembling. 
// The matrix is splitted in 4 blocks. 

//----------------------------------------------------------------------------------------------------------------------------//
// First block
//   - First block includes only the first term of the equation 
//                      (curl_{beta}(W^{beta}),sigma_bar curl_{beta}(N^{beta}))
//     that are independent of beta.
//
//  ******** SYSTEM LAYOUT 2D ********
//  
//  In the paper (u,v,w) = (H_x,H_y,H_z)
//
//             u     v     w
//          |-----------------|       |-----------------|
//       u  |  0  |  1  |  2  |       |  X  |  0  |  X  |
//          |-----------------|       |-----------------|
//       v  |  3  |  4  |  5  |   =>  |  0  |  X  |  0  |
//          |-----------------|       |-----------------|
//       w  |  6  |  7  |  8  |       |  X  |  0  |  X  |
//          |-----------------|       |-----------------|
//
//  ***********************************
#undef  __FUNCT__
#define __FUNCT__ "MatrixLHS_sigma_bar"
PetscErrorCode MatrixLHS_sigma_bar(IGAMPoint q,PetscScalar *K[],void *ctx)
{
  PetscErrorCode ierr;
  Params *user  = (Params*)ctx;
  PetscInt  a,b;
  PetscInt  dim,nen;
  PetscInt uu[3][3] = {{0,1,2},{3,4,5},{6,7,8}};

  if (q->g_point->parent->atboundary) goto boundary;
  {
    //----------------------------------------------//
    // Vectorial shape functions (Hcurl)
    PetscInt  nfld_curl;
    IGAPoint  *fld_curl    = NULL;
    PetscInt  *fldidx_curl = NULL;
    PetscReal **N_curl     = NULL;
    PetscReal **dNdX_curl  = NULL;
  
    dim         = q->ds_point[0]->dim;
    nfld_curl   = q->ds_point[0]->numfields;
    fldidx_curl = q->ds_point[0]->fields_idx;
    nen         = q->f_point[fldidx_curl[0]]->nen;
  
    ierr  = PetscMalloc(sizeof(IGAPoint)   *nfld_curl,&fld_curl);CHKERRQ(ierr);
    ierr  = PetscMalloc(sizeof(PetscReal *)*nfld_curl,&N_curl  );CHKERRQ(ierr);
    ierr  = PetscMalloc(sizeof(PetscReal *)*nfld_curl,&dNdX_curl);CHKERRQ(ierr);
  
    for(b=0;b<nfld_curl;b++){
      fld_curl[b]    =  q->f_point[fldidx_curl[b]];
      N_curl[b]      =  fld_curl[b]->basis[0];
      dNdX_curl[b]   =  fld_curl[b]->basis[1];
    }
    
    const IGAPoint x = q->g_point;
    const PetscReal (*DFinv) [dim]      = (typeof(DFinv))  x->mapU[1];

    //----------------------------------------------//
    // Point localization
    PetscReal xyz[2];
    IGAPointFormGeomMap(x,xyz);
  
    // Sigma value
    PetscInt loc=0;
    fsigma_loc(xyz,&loc,ctx);
  
    //----------------------------------------------//
    //Mapp functions
    PetscReal   dNxdz[nen],dNzdx[nen];
    PetscScalar dNxdz_curl[nen],dNzdx_curl[nen];
    PetscReal (*dNdX_X)[dim] = (typeof(dNdX_X))dNdX_curl[0];
    PetscReal (*dNdX_Z)[dim] = (typeof(dNdX_Z))dNdX_curl[1];
    for (a=0; a<nen; a++) {
      dNxdz[a]      = DFinv[0][0]*(dNdX_X[a][1]*DFinv[1][1]);
      dNzdx[a]      = DFinv[1][1]*(dNdX_Z[a][0]*DFinv[0][0]);
      dNxdz_curl[a] = user->sigma_bar[0][loc]*dNxdz[a]; 
      dNzdx_curl[a] = user->sigma_bar[0][loc]*dNzdx[a]; //user->sigma_bar[1][loc]
    }

    //----------------------------------------------//
    // Scalar shape functions (H1)
  
    IGAPoint  fld_H1;
    PetscInt  fldidx_H1;
  
    dim       = q->ds_point[1]->dim;
    nen       = q->ds_point[1]->nen;
    fldidx_H1 = q->ds_point[1]->fields_idx[0];
    
    fld_H1    = q->f_point[fldidx_H1];
    
    //Mapp functions
    PetscReal dNydx[nen],dNydz[nen];
    PetscScalar dNydx_h1[nen],dNydz_h1[nen];
    PetscReal (*dNdX_H1)[dim] = (typeof(dNdX_H1)) fld_H1->basis[1];
    for (a=0; a<nen; a++) {
      dNydx[a]    = dNdX_H1[a][0]*DFinv[0][0];
      dNydz[a]    = dNdX_H1[a][1]*DFinv[1][1];
      dNydx_h1[a] = user->sigma_bar[1][loc]*dNydx[a];
      dNydz_h1[a] = user->sigma_bar[0][loc]*dNydz[a];
    }
  
    //----------------------------------------------//
    // Equation to solve
    const IGAPoint *p = q->f_point;
    PetscScalar (*Juu)[p[0]->nen] = (typeof(Juu)) K[uu[0][0]];
    PetscScalar (*Juw)[p[0]->nen] = (typeof(Juw)) K[uu[2][0]];
    PetscScalar (*Jvv)[p[1]->nen] = (typeof(Jvv)) K[uu[1][1]];
    PetscScalar (*Jwu)[p[2]->nen] = (typeof(Jwu)) K[uu[0][2]];
    PetscScalar (*Jww)[p[2]->nen] = (typeof(Jww)) K[uu[2][2]];
    //
    for (a=0; a<p[0]->nen; a++) {
      for (b=0; b<p[0]->nen; b++){
        Juu[a][b] +=  (dNxdz[b]*dNxdz_curl[a]);
        Juw[a][b] += -(dNxdz[b]*dNzdx_curl[a]);
        Jwu[a][b] += -(dNzdx[b]*dNxdz_curl[a]);
        Jww[a][b] +=  (dNzdx[b]*dNzdx_curl[a]);
      }
    }
    //
    for (a=0; a<p[1]->nen; a++) {
      for (b=0; b<p[1]->nen; b++){
        Jvv[a][b] += (dNydx[b]*dNydx_h1[a])+(dNydz[b]*dNydz_h1[a]);
      }
    }
  }
  // Weak boundary conditions
  boundary:;
  // No weak boundary condition included thus this part is empty
  {}
  return 0;
}

//----------------------------------------------------------------------------------------------------------------------------//
// Second block
//   - Second block includes only the second term of the equation 
//                      (W^{beta},mu_bar N^{beta})
//
//  ******** SYSTEM LAYOUT 2D ********
//  
//  In the paper (u,v,w) = (H_x,H_y,H_z)
//
//             u     v     w
//          |-----------------|       |-----------------|
//       u  |  0  |  1  |  2  |       |  X  |  0  |  0  |
//          |-----------------|       |-----------------|
//       v  |  3  |  4  |  5  |   =>  |  0  |  X  |  0  |
//          |-----------------|       |-----------------|
//       w  |  6  |  7  |  8  |       |  0  |  0  |  X  |
//          |-----------------|       |-----------------|
//
//  ***********************************
#undef  __FUNCT__
#define __FUNCT__ "MatrixLHS_mu_bar"
PetscErrorCode MatrixLHS_mu_bar(IGAMPoint q,PetscScalar *K[],void *ctx)
{
  PetscErrorCode ierr;
  PetscInt  a,b;
  PetscInt  dim,nen;
  PetscInt uu[3][3] = {{0,1,2},{3,4,5},{6,7,8}};

  if (q->g_point->parent->atboundary) goto boundary;
  {
    //----------------------------------------------//
    // Vectorial shape functions (Hcurl)
    PetscInt  nfld_curl;
    IGAPoint  *fld_curl    = NULL;
    PetscInt  *fldidx_curl = NULL;
    PetscReal **N_curl     = NULL;
  
    dim         = q->ds_point[0]->dim;
    nfld_curl   = q->ds_point[0]->numfields;
    fldidx_curl = q->ds_point[0]->fields_idx;
    nen         = q->f_point[fldidx_curl[0]]->nen;
  
    ierr  = PetscMalloc(sizeof(IGAPoint)   *nfld_curl,&fld_curl);CHKERRQ(ierr);
    ierr  = PetscMalloc(sizeof(PetscReal *)*nfld_curl,&N_curl  );CHKERRQ(ierr);
  
    for(b=0;b<nfld_curl;b++){
      fld_curl[b]    =  q->f_point[fldidx_curl[b]];
      N_curl[b]      =  fld_curl[b]->basis[0];
    }
    
    const IGAPoint x = q->g_point;
    const PetscReal (*DFinv) [dim]      = (typeof(DFinv))  x->mapU[1];

    //----------------------------------------------//
    // Point localization
    PetscReal xyz[2];
    IGAPointFormGeomMap(x,xyz);
  
    // Sigma value
    PetscInt loc=0;
    fsigma_loc(xyz,&loc,ctx);
  
    //----------------------------------------------//
    //Mapp functions  
    PetscReal   Nx[nen],Nz[nen];
    for (a=0; a<nen; a++) {
      Nx[a]         = DFinv[0][0]*N_curl[0][a];
      Nz[a]         = DFinv[1][1]*N_curl[1][a];
    }

    //----------------------------------------------//
    // Scalar shape functions (H1)
  
    IGAPoint  fld_H1;
    PetscInt  fldidx_H1;
    
    dim       = q->ds_point[1]->dim;
    nen       = q->ds_point[1]->nen;
    fldidx_H1 = q->ds_point[1]->fields_idx[0];
    
    fld_H1    = q->f_point[fldidx_H1];
    
    //Mapp functions
    PetscReal (*N_H1)         = (typeof(   N_H1)) fld_H1->basis[0];
  
    //----------------------------------------------//
    // Equation to solve
    const IGAPoint *p = q->f_point;
    PetscScalar (*Juu)[p[0]->nen] = (typeof(Juu)) K[uu[0][0]];
    PetscScalar (*Jvv)[p[1]->nen] = (typeof(Jvv)) K[uu[1][1]];
    PetscScalar (*Jww)[p[2]->nen] = (typeof(Jww)) K[uu[2][2]];
    //
    for (a=0; a<p[0]->nen; a++) {
      for (b=0; b<p[0]->nen; b++){
        Juu[a][b] +=  Nx[b]*Nx[a];
        Jww[a][b] +=  Nz[b]*Nz[a];
      }
    }
    //
    for (a=0; a<p[1]->nen; a++) {
      for (b=0; b<p[1]->nen; b++){
        Jvv[a][b] += N_H1[b]*N_H1[a];
      }
    }
  }
  // Weak boundary conditions
  boundary:;
  // No weak boundary condition included thus this part is empty
  {}
  return 0;
}

//----------------------------------------------------------------------------------------------------------------------------//
// Third block
//
//  ******** SYSTEM LAYOUT 2D ********
//  
//  In the paper (u,v,w) = (H_x,H_y,H_z)
//
//             u     v     w
//          |-----------------|       |-----------------|
//       u  |  0  |  1  |  2  |       |  0  |  X  |  0  |
//          |-----------------|       |-----------------|
//       v  |  3  |  4  |  5  |   =>  |  X  |  0  |  X  |
//          |-----------------|       |-----------------|
//       w  |  6  |  7  |  8  |       |  0  |  X  |  0  |
//          |-----------------|       |-----------------|
//
//  ***********************************
#undef  __FUNCT__
#define __FUNCT__ "MatrixLHS_beta_linear"
PetscErrorCode MatrixLHS_beta_linear(IGAMPoint q,PetscScalar *K[],void *ctx)
{
  PetscErrorCode ierr;
  Params *user  = (Params*)ctx;
  PetscInt  a,b;
  PetscInt  dim,nen;
  PetscInt uu[3][3] = {{0,1,2},{3,4,5},{6,7,8}};

  if (q->g_point->parent->atboundary) goto boundary;
  {
    //----------------------------------------------//
    // Vectorial shape functions (Hcurl)
    PetscInt  nfld_curl;
    IGAPoint  *fld_curl    = NULL;
    PetscInt  *fldidx_curl = NULL;
    PetscReal **N_curl     = NULL;
  
    dim         = q->ds_point[0]->dim;
    nfld_curl   = q->ds_point[0]->numfields;
    fldidx_curl = q->ds_point[0]->fields_idx;
    nen         = q->f_point[fldidx_curl[0]]->nen;
  
    ierr  = PetscMalloc(sizeof(IGAPoint)   *nfld_curl,&fld_curl);CHKERRQ(ierr);
    ierr  = PetscMalloc(sizeof(PetscReal *)*nfld_curl,&N_curl  );CHKERRQ(ierr);
  
    for(b=0;b<nfld_curl;b++){
      fld_curl[b]    =  q->f_point[fldidx_curl[b]];
      N_curl[b]      =  fld_curl[b]->basis[0];
    }
    
    const IGAPoint x = q->g_point;
    const PetscReal (*DFinv) [dim]      = (typeof(DFinv))  x->mapU[1];

    //----------------------------------------------//
    // Point localization
    PetscReal xyz[2];
    IGAPointFormGeomMap(x,xyz);
  
    // Sigma value
    PetscInt loc=0;
    fsigma_loc(xyz,&loc,ctx);
  
    //----------------------------------------------//
    //Mapp functions  
    PetscReal   Nx[nen],Nz[nen];
    PetscScalar Nx_h1_curl[nen],Nz_h1_curl[nen];
    for (a=0; a<nen; a++) {
      Nx[a]         = DFinv[0][0]*N_curl[0][a];
      Nz[a]         = DFinv[1][1]*N_curl[1][a];
      Nx_h1_curl[a] = user->sigma_bar[1][loc]*Nx[a];
      Nz_h1_curl[a] = user->sigma_bar[0][loc]*Nz[a];
    }

    //----------------------------------------------//
    // Scalar shape functions (H1)
  
    IGAPoint  fld_H1;
    PetscInt  fldidx_H1;
    
    dim       = q->ds_point[1]->dim;
    nen       = q->ds_point[1]->nen;
    fldidx_H1 = q->ds_point[1]->fields_idx[0];
    
    fld_H1    = q->f_point[fldidx_H1];
    
    //Mapp functions
    PetscReal dNydx[nen],dNydz[nen];
    PetscReal (*N_H1)         = (typeof(   N_H1)) fld_H1->basis[0];
    PetscReal (*dNdX_H1)[dim] = (typeof(dNdX_H1)) fld_H1->basis[1];
    for (a=0; a<nen; a++) {
      dNydx[a]    = dNdX_H1[a][0]*DFinv[0][0];
      dNydz[a]    = dNdX_H1[a][1]*DFinv[1][1];
    }
  
    //----------------------------------------------//
    // Equation to solve
    const IGAPoint *p = q->f_point;
    PetscScalar (*Juv)[p[0]->nen] = (typeof(Juv)) K[uu[1][0]];
    PetscScalar (*Jvu)[p[1]->nen] = (typeof(Jvu)) K[uu[0][1]];
    PetscScalar (*Jvw)[p[1]->nen] = (typeof(Jvw)) K[uu[2][1]];
    PetscScalar (*Jwv)[p[2]->nen] = (typeof(Jwv)) K[uu[1][2]];
    //
    for (a=0; a<p[0]->nen; a++) {
      for (b=0; b<p[1]->nen; b++){
        Jvu[a][b] += -(dNydx[b]*Nx_h1_curl[a]);
        Jvw[a][b] += -(dNydz[b]*Nz_h1_curl[a]);
        Juv[b][a] +=  (Nx_h1_curl[a]*dNydx[b]);
        Jwv[b][a] +=  (Nz_h1_curl[a]*dNydz[b]);
      }
    }
  }
  // Weak boundary conditions
  boundary:;
  // No weak boundary condition included thus this part is empty
  {}
  return 0;
}

//----------------------------------------------------------------------------------------------------------------------------//
// Fourth block
//
//  ******** SYSTEM LAYOUT 2D ********
//  
//  In the paper (u,v,w) = (H_x,H_y,H_z)
//
//             u     v     w
//          |-----------------|       |-----------------|
//       u  |  0  |  1  |  2  |       |  X  |  0  |  0  |
//          |-----------------|       |-----------------|
//       v  |  3  |  4  |  5  |   =>  |  0  |  0  |  0  |
//          |-----------------|       |-----------------|
//       w  |  6  |  7  |  8  |       |  0  |  0  |  X  |
//          |-----------------|       |-----------------|
//
//  ***********************************
#undef  __FUNCT__
#define __FUNCT__ "MatrixLHS_beta_quadratic"
PetscErrorCode MatrixLHS_beta_quadratic(IGAMPoint q,PetscScalar *K[],void *ctx)
{
  PetscErrorCode ierr;
  Params *user  = (Params*)ctx;
  PetscInt  a,b;
  PetscInt  dim,nen;
  PetscInt uu[3][3] = {{0,1,2},{3,4,5},{6,7,8}};

  if (q->g_point->parent->atboundary) goto boundary;
  {
    //----------------------------------------------//
    // Vectorial shape functions (Hcurl)
    PetscInt  nfld_curl;
    IGAPoint  *fld_curl    = NULL;
    PetscInt  *fldidx_curl = NULL;
    PetscReal **N_curl     = NULL;
  
    dim         = q->ds_point[0]->dim;
    nfld_curl   = q->ds_point[0]->numfields;
    fldidx_curl = q->ds_point[0]->fields_idx;
    nen         = q->f_point[fldidx_curl[0]]->nen;
  
    ierr  = PetscMalloc(sizeof(IGAPoint)   *nfld_curl,&fld_curl);CHKERRQ(ierr);
    ierr  = PetscMalloc(sizeof(PetscReal *)*nfld_curl,&N_curl  );CHKERRQ(ierr);
  
    for(b=0;b<nfld_curl;b++){
      fld_curl[b]    =  q->f_point[fldidx_curl[b]];
      N_curl[b]      =  fld_curl[b]->basis[0];
    }
    
    const IGAPoint x = q->g_point;
    const PetscReal (*DFinv) [dim]      = (typeof(DFinv))  x->mapU[1];

    //----------------------------------------------//
    // Point localization
    PetscReal xyz[2];
    IGAPointFormGeomMap(x,xyz);
  
    // Sigma value
    PetscInt loc=0;
    fsigma_loc(xyz,&loc,ctx);
  
    //----------------------------------------------//
    //Mapp functions  
    PetscReal   Nx[nen],Nz[nen];
    PetscScalar Nx_curl[nen],Nz_curl[nen];
    for (a=0; a<nen; a++) {
      Nx[a]         = DFinv[0][0]*N_curl[0][a];
      Nz[a]         = DFinv[1][1]*N_curl[1][a];
      Nx_curl[a]    = user->sigma_bar[1][loc]*Nx[a];
      Nz_curl[a]    = user->sigma_bar[0][loc]*Nz[a];
    }
  
    //----------------------------------------------//
    // Equation to solve
    const IGAPoint *p = q->f_point;
    PetscScalar (*Juu)[p[0]->nen] = (typeof(Juu)) K[uu[0][0]];
    PetscScalar (*Jww)[p[2]->nen] = (typeof(Jww)) K[uu[2][2]];
    //
    for (a=0; a<p[0]->nen; a++) {
      for (b=0; b<p[0]->nen; b++){
        Juu[a][b] += Nx[b]*Nx_curl[a];
        Jww[a][b] += Nz[b]*Nz_curl[a];
      }
    }
  }
  // Weak boundary conditions
  boundary:;
  // No weak boundary condition included thus this part is empty
  {}
  return 0;
}
//----------------------------------------------------------------------------------------------------------------------------//
//----------------------------------------------------------------------------------------------------------------------------//
//----------------------------------------------------------------------------------------------------------------------------//


//----------------------------------------------------------------------------------------------------------------------------//
// - Subroutines requires to assemble the modified RHS
PETSC_STATIC_INLINE
PetscBool IGAMElementNextFormVector(IGAMElement element,IGAMFormVector *vec,void **ctx)
{
  IGAMForm form = element->parent->form;
  if (!IGAMElementNextForm(element,form->visit)) return PETSC_FALSE;
  *vec = form->ops->Vector;
  *ctx = form->ops->VecCtx;
  return PETSC_TRUE;
}

//----------------------------------------------------------------------------------------------------------------------------//
// Subroutine used to compute the modified RHS
PetscErrorCode IGAMComputeRHS(IGAM iga,Vec vecB,void *ctxg)
{
  Vec            *vecsB;
  IGAMElement    element;
  IGAMPoint      point;
  IGAMFormVector Vector;
  void           *ctx;
  Params         *user = (Params*)ctxg;
  PetscScalar    **B;
  PetscScalar    **F;
  PetscErrorCode ierr;
  PetscFunctionBegin;
  PetscValidHeaderSpecific(iga,IGAM_CLASSID,1);
  PetscValidHeaderSpecific(vecB,VEC_CLASSID,2);

  ierr = VecZeroEntries(vecB);CHKERRQ(ierr);
  ierr = IGAMUnpackVec(iga,vecB,&vecsB);CHKERRQ(ierr);

  ierr = PetscLogEventBegin(IGA_FormVector,iga,vecB,0,0);CHKERRQ(ierr);

  /* Element loop */
  ierr = IGAMBeginElement(iga,&element);CHKERRQ(ierr);
  while (IGAMNextElement(iga,element)) {
    /* Set if element is evaluable */
    if ( user->srczone[element->g_element->index] == user->sr_id ) {
      ierr = IGAMElementGetWorkVec(element,&B);CHKERRQ(ierr);
      /* FormVector loop */
      while (IGAMElementNextFormVector(element,&Vector,&ctx)) {
        /* Quadrature loop */
        ierr = IGAMElementBeginPoint(element,&point);CHKERRQ(ierr);
        while (IGAMElementNextPoint(element,point)) {
          ierr = IGAMPointGetWorkVec(point,&F);CHKERRQ(ierr);
          ierr = Vector(point,F,ctx);CHKERRQ(ierr);
          ierr = IGAMPointAddVec(point,F,B);CHKERRQ(ierr);
        }
        ierr = IGAMElementEndPoint(element,&point);CHKERRQ(ierr);
      }
      ierr = IGAMElementAssembleVec(element,B,vecsB);CHKERRQ(ierr);
    }
  }
  ierr = IGAMEndElement(iga,&element);CHKERRQ(ierr);

  ierr = PetscLogEventEnd(IGA_FormVector,iga,vecB,0,0);CHKERRQ(ierr);

  ierr = IGAMRepackVec(iga,vecB,&vecsB);CHKERRQ(ierr);
  ierr = VecAssemblyBegin(vecB);CHKERRQ(ierr);
  ierr = VecAssemblyEnd  (vecB);CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

//----------------------------------------------------------------------------------------------------------------------------//
// Matrix formulation of the system problem
// This subroutine assemble the vector f of the system Ku=f
#undef  __FUNCT__
#define __FUNCT__ "VectorRHS"
PetscErrorCode VectorRHS(IGAMPoint q,PetscScalar *F[],void *ctx)
{
  PetscErrorCode ierr;

  Params *user  = (Params*)ctx;
     
  PetscInt  i,a,b;
  PetscInt  dim,nen;

  if (q->g_point->parent->atboundary) goto boundary;
  {
    //----------------------------------------------//
    // Check if we are in the source element
    IGAElement element; 
    element = q->parent->g_element;

 
  if ( user->srczone[element->index] == user->sr_id ) 
    {
      //----------------------------------------------//
      // Vectorial shape functions (Hcurl)
      PetscInt  nfld_curl;
      IGAPoint  *fld_curl    = NULL;
      PetscInt  *fldidx_curl = NULL;
      PetscReal **N_curl     = NULL;
      
      dim         = q->ds_point[0]->dim;
      nfld_curl   = q->ds_point[0]->numfields;
      fldidx_curl = q->ds_point[0]->fields_idx;
      nen         = q->f_point[fldidx_curl[0]]->nen;
      
      ierr  = PetscMalloc(sizeof(IGAPoint)   *nfld_curl,&fld_curl);CHKERRQ(ierr);
      ierr  = PetscMalloc(sizeof(PetscReal *)*nfld_curl,&N_curl  );CHKERRQ(ierr);
      
      for(b=0;b<nfld_curl;b++){
        fld_curl[b]    =  q->f_point[fldidx_curl[b]];
        N_curl[b]      =  fld_curl[b]->basis[0];
      }
      
      const IGAPoint x = q->g_point;
      const PetscReal (*DFinv) [dim]      = (typeof(DFinv))  x->mapU[1];
  
      //Mapp functions  
      PetscReal Nx[nen],Nz[nen];
      for (a=0; a<nen; a++) {
        Nx[a] = DFinv[0][0]*N_curl[0][a];
        Nz[a] = DFinv[1][1]*N_curl[1][a];
      }
    
      //----------------------------------------------//
      // Scalar shape functions (H1)
    
      IGAPoint  fld_H1;
      PetscInt  fldidx_H1;
    
      dim       = q->ds_point[1]->dim;
      nen       = q->ds_point[1]->nen;
      fldidx_H1 = q->ds_point[1]->fields_idx[0];
    
      fld_H1    = q->f_point[fldidx_H1];
      
      PetscReal (*N_H1) = (typeof(   N_H1)) fld_H1->basis[0];
    
      //----------------------------------------------//
      // Point localization
      PetscReal xyz[2];
      IGAPointFormGeomMap(x,xyz);
      // Forcing
      PetscScalar f[3] = {0,0,0};
      Forcing(xyz,f,ctx);
      // Compute Area
      PetscReal Area = 0;
      for (i = 0; i < element->nqp; ++i){Area = Area + element->weight[i]*element->detJac[i];}

      //----------------------------------------------//
      // Equation to solve
      const IGAPoint *p = q->f_point;
      //
      for (a=0; a<p[0]->nen; a++) {
        F[0][a] += (-user->mu_bar*Nx[a]*f[0]/Area);
        F[2][a] += (-user->mu_bar*Nz[a]*f[2]/Area);
      }
      //
      for (a=0; a<p[1]->nen; a++) {
        F[1][a] += (-user->mu_bar*N_H1[a]*f[1]/Area);
      }
    }
  }
  // Weak boundary conditions
  boundary:;
  // No weak boundary condition included thus this part is empty
  {}
  return 0;
}

//----------------------------------------------------------------------------------------------------------------------------//
// - Subroutines requires to compute puntal data 
// Set Basis
extern void IGA_Basis_BSpline (PetscInt i,PetscReal u,PetscInt p,PetscInt d,const PetscReal U[],PetscReal B[]);

//----------------------------------------------------------------------------------------------------------------------------//
// Get values/gradient/hessian/...
extern void IGA_GetValue(PetscInt nen,PetscInt dof,/*         */const PetscReal N[],const PetscScalar U[],PetscScalar u[]);

//----------------------------------------------------------------------------------------------------------------------------//
// Subroutine used to compute the span of a particular basis function
PetscInt IGA_FindSpan(PetscInt n,PetscInt p,PetscReal u, const PetscReal U[])
{
  PetscInt low,high,span;
  if (PetscUnlikely(u <= U[p]))   return p;
  if (PetscUnlikely(u >= U[n+1])) return n;
  low  = p;
  high = n+1;
  span = (high+low)/2;
  while (u < U[span] || u >= U[span+1]) {
    if (u < U[span]) {
      high = span;
    } else {
      low = span;
    }
    span = (high+low)/2;
  }
  return span;
}

//----------------------------------------------------------------------------------------------------------------------------//
// Subroutine used to compute point values
#undef  __FUNCT__
#define __FUNCT__ "GetRValue"
PetscErrorCode GetRValue(IGAM iga, PetscInt fld, Vec x, PetscReal point_io[], PetscScalar u_io[])
{ 
  // NOTE: point has the dimension of 2 and contains the values x,z.
  PetscReal   point[2];
  PetscScalar u[2];
  //----------------------------------------------//
  // Variables
  PetscInt i,j,k,aux,aux2;
  PetscBool offprocess[fld+1];
  PetscBool collective = PETSC_FALSE;
  // Vectorial data
  Vec lvec[fld];
  Vec gvec[fld];
  
  const PetscScalar *arrayA[fld];
  // Geometry data
  const PetscReal   *arrayW; 
  const PetscReal   *arrayX; 
  // Dimension of the problem
  PetscInt     dim;
  PetscInt     nsd;
  dim = iga->g_iga->dim; 
  nsd = iga->g_iga->geometry ? iga->g_iga->geometry : iga->g_iga->dim;
  // Polynomial order
  PetscInt     order[fld+1];
  // Degrees of freedom
  PetscInt     dof=1; // We are use one dof per space
  // Knot vector and polynomial order
  PetscReal   *U[fld+1][nsd];
  PetscInt     p[fld+1][nsd];
  PetscInt     n[fld+1][nsd];
  PetscInt     s[fld+1][nsd];
  PetscInt     w[fld+1][nsd];
  // Number of local basis functions
  PetscInt     nen[fld+1];
  // Mappings
  PetscInt    *map[fld+1];
  // Local dof nodal values
  PetscScalar *A[fld];
  // Local rational 
  PetscReal   *W;
  // Local geometry data
  PetscReal   *X;
  PetscInt     ndiff = 5;
  PetscReal   *gbasis[1][ndiff];
  PetscReal   *mapU[ndiff];
  PetscReal   *mapX[ndiff];
  PetscReal   *detX;
  // FE 
  PetscInt     ID[fld+1][3];
  PetscReal   *BD[fld+1][nsd];
  PetscReal   *fbasis[fld][ndiff];

  PetscErrorCode ierr;

  //----------------------------------------------//
  // Set point vector
  point[0] = point_io[0]; // x
  point[1] = point_io[2]; // z

  //----------------------------------------------//
  // Extract the FE solution value per the fields (Hx,Hy and Hz)
  Vec *vecs;
  ierr = IGAMUnpackVec(iga,x,&vecs);CHKERRQ(ierr);
  for (i=0; i<fld; i++) {
    ierr = VecDuplicate(vecs[i],&gvec[i]);CHKERRQ(ierr);
    ierr = IGAGetOrder(iga->f_iga[i],&order[i]);CHKERRQ(ierr);
    ierr = VecCopy(vecs[i],gvec[i]);CHKERRQ(ierr);
    // This lines gives the array and local vec (in case we use parallel solution)
    ierr = IGAGetLocalVecArray(iga->f_iga[i],gvec[i],&lvec[i],&arrayA[i]);CHKERRQ(ierr); 
  }
  ierr = IGAMRepackVec(iga,x,&vecs);CHKERRQ(ierr);
  
  //----------------------------------------------//
  // Fields BD allocation
  for (i=0; i<fld; i++) {
    // Initialize number of local basis functions
    nen[i]=1;
    // Loop on dimensions
    for (j=0; j<nsd; j++) {
      p[i][j] = iga->f_iga[i]->axis[j]->p;     // Polynomial order
      U[i][j] = iga->f_iga[i]->axis[j]->U;     // Knot vector
      n[i][j] = iga->f_iga[i]->geom_sizes[j];  // Size of the 1D geom
      s[i][j] = iga->f_iga[i]->geom_gstart[j]; // Global start
      w[i][j] = iga->f_iga[i]->geom_gwidth[j]; // Global width
      nen[i] *= (p[i][j]+1);                   // Number of local basis functions
      ierr = PetscMalloc1((size_t)((p[i][j]+1)*5),&BD[i][j]);CHKERRQ(ierr);
    }
    // Allocate memory
    ierr = PetscCalloc1(nen[i]/**/,&map[i]/*    */);CHKERRQ(ierr);
    ierr = PetscCalloc1(nen[i]*dof,&A[i]/*      */);CHKERRQ(ierr);
    for (j = 0; j < ndiff; j++){
      aux  = pow(dim,j);
      ierr = PetscCalloc1(nen[i]*aux,&fbasis[i][j]);CHKERRQ(ierr);
    }
  }

  //----------------------------------------------//
  // Geometry BD allocation
  // Initialize number of local basis functions
  nen[fld]=1;
  // Loop on dimensions
  for (j=0; j<nsd; j++) {
    p[fld][j] = iga->g_iga->axis[j]->p;     // Polynomial order
    U[fld][j] = iga->g_iga->axis[j]->U;     // Knot vector
    n[fld][j] = iga->g_iga->geom_sizes[j];  // Size of the 1D geom
    s[fld][j] = iga->g_iga->geom_gstart[j]; // Global start
    w[fld][j] = iga->g_iga->geom_gwidth[j]; // Global width
    nen[fld] *= (p[fld][j]+1);              // Number of local basis functions
    ierr = PetscMalloc1((size_t)((p[fld][j]+1)*5),&BD[fld][j]);CHKERRQ(ierr);
  }
  // Allocate memory
  ierr = PetscCalloc1(nen[fld]/**/,&map[fld]);CHKERRQ(ierr);
  ierr = PetscCalloc1(nen[fld]/**/,&W/*   */);CHKERRQ(ierr);
  ierr = PetscCalloc1(nen[fld]*nsd,&X/*   */);CHKERRQ(ierr);
  for (j=0; j<ndiff; j++) 
  {
    aux  = pow(dim,j);
    aux2 = pow(nsd,j);
    ierr = PetscCalloc1(nen[fld]*aux,&gbasis[0][j]);CHKERRQ(ierr);
    ierr = PetscCalloc1(dim*aux2/**/,&mapU[j]  );CHKERRQ(ierr);
    ierr = PetscCalloc1(nsd*aux/* */,&mapX[j]  );CHKERRQ(ierr);
  }
  ierr = PetscCalloc1(1,&detX);CHKERRQ(ierr);
  ierr = IGAGetOrder(iga->g_iga,&order[fld]);CHKERRQ(ierr);
  //
  detX[0] = 1.0;
  for (j=0; j<dim; j++) mapU[1][j*(nsd+1)] = 1.0;
  for (j=0; j<nsd; j++) mapX[1][j*(dim+1)] = 1.0;

  //----------------------------------------------//
  // Span index (obtain the set of basis functions indexes required to compute the point value)
  for (i=0; i<fld+1; i++){
    for (j=0; j<nsd; j++){ID[i][j] = IGA_FindSpan(n[i][j]-1,p[i][j],point[j],U[i][j]);}
    offprocess[i] = PETSC_FALSE;
    for (j=0; j<nsd; j++) {
      PetscInt first = ID[i][j]-p[i][j], last = first + p[i][j];
      PetscInt start = s[i][j], end = s[i][j] + w[i][j];
      if (first < start || last >= end) offprocess[i] = PETSC_TRUE;
    }
    if (PetscUnlikely(offprocess[i] && !collective)) PetscFunctionReturn(0);
  }

  //----------------------------------------------//
  // Span closure (Field)
  for (i=0; i<fld; i++) {
    if (PetscLikely(!offprocess[i])) {
      PetscInt ia, inen, ioffset, istart;
      PetscInt ja, jnen, joffset, jstart;
      PetscInt pos, jstride;//, kstride;
      PetscInt iA, jA;//, kA;
      //
      PetscInt *IDf = ID[i], *pf = p[i], *sf = s[i], *wf = w[i];
      inen = pf[0]+1; ioffset = IDf[0]-pf[0]; istart = sf[0];
      jnen = pf[1]+1; joffset = IDf[1]-pf[1]; jstart = sf[1];
      pos = 0, jstride = wf[0];//, kstride = wf[0]*wf[1];
      // Build map to go from global to local basis functions
    ierr = PetscFree(W/*   */);CHKERRQ(ierr);
    //for (ka=0; ka<knen; ka++) {
        for (ja=0; ja<jnen; ja++) {
          for (ia=0; ia<inen; ia++) {
            iA = (ioffset + ia) - istart;
            jA = (joffset + ja) - jstart;
            map[i][pos++] = iA + jA*jstride;// + kA*kstride;
          }
        }
      //}
      // Obtain the nodal value (dof values) of the local basis fucntions required to compute the point value
      if (arrayA[i]){for (k=0; k<nen[i]; k++){for (j=0; j<dof; j++){A[i][j + k*dof] = arrayA[i][j + map[i][k]*dof];}}}
    }

  //----------------------------------------------//
  // Compute 1D basis functions
    void (*ComputeBasis)(PetscInt,PetscReal,PetscInt,PetscInt,const PetscReal[],PetscReal[]) = NULL;
    for (j=0; j<dim; j++) {
      ComputeBasis = IGA_Basis_BSpline;
      ComputeBasis(ID[i][j],point[j],p[i][j],order[i],U[i][j],BD[i][j]);
    }    

    //----------------------------------------------//
    // Tensor product of 1D basis functions to build a 2D basis function
      IGA_BasisFuns_2D(order[i],1,p[i][0]+1,BD[i][0],
                                1,p[i][1]+1,BD[i][1],
                                fbasis[i][0],fbasis[i][1],fbasis[i][2],fbasis[i][3],fbasis[i][4]);
  }

  //----------------------------------------------//
  // Span closure (Geometry)
  {
    if (PetscLikely(!offprocess[i])) {
      PetscInt ia, inen, ioffset, istart;
      PetscInt ja, jnen, joffset, jstart;
      PetscInt pos, jstride;//, kstride;
      PetscInt iA, jA;//, kA;
      //
      PetscInt *IDg = ID[fld], *pg = p[fld], *sg = s[fld], *wg = w[fld];
      inen = pg[0]+1; ioffset = IDg[0]-pg[0]; istart = sg[0];
      jnen = pg[1]+1; joffset = IDg[1]-pg[1]; jstart = sg[1];
      pos = 0, jstride = wg[0];//, kstride = wg[0]*wg[1];
      // Build map to go from global to local basis functions
        for (ja=0; ja<jnen; ja++) {
          for (ia=0; ia<inen; ia++) {
            iA = (ioffset + ia) - istart;
            jA = (joffset + ja) - jstart;
            map[fld][pos++] = iA + jA*jstride;// + kA*kstride;
          }
        }
      // If rational
      if (iga->g_iga->rational && iga->g_iga->rationalW){
        arrayW = iga->g_iga->rationalW;
        for (j=0; j<nen[fld]; j++){W[j] = arrayW[map[fld][j]];}
      }
      // If geometry
      if (iga->g_iga->geometry && iga->g_iga->geometryX){
        arrayX = iga->g_iga->geometryX;
        for (k=0; k<nen[fld]; k++){for (j=0; j<nsd; j++){X[j + k*nsd] = arrayX[j + map[fld][k]*nsd];}}
      }
    }


    //----------------------------------------------//
    // Compute 1D basis functions
    void (*ComputeBasis)(PetscInt,PetscReal,PetscInt,PetscInt,const PetscReal[],PetscReal[]) = NULL;
    for (j=0; j<dim; j++) {
      ComputeBasis = IGA_Basis_BSpline;
      ComputeBasis(ID[fld][j],point[j],p[fld][j],order[fld],U[fld][j],BD[fld][j]);
    }    

    //----------------------------------------------//
    // Tensor product of 1D basis functions to build a 2D basis function
    IGA_BasisFuns_2D(order[fld],1,p[fld][0]+1,BD[fld][0],
                                1,p[fld][1]+1,BD[fld][1],
                                gbasis[0][0],gbasis[0][1],gbasis[0][2],gbasis[0][3],gbasis[0][4]);

    //----------------------------------------------//U
    // Rationalize basis functions
    if (iga->g_iga->rational && iga->g_iga->rationalW){
      IGA_Rationalize_2D(order[fld],1,nen[fld],W,gbasis[0][0],gbasis[0][1],gbasis[0][2],gbasis[0][3],gbasis[0][4]);
    }
    
    //----------------------------------------------//
    // Geometry mapping
    for (i=0; i<dim; i++){mapU[0][i] = point[i];mapX[0][i] = point[i];}
    if (iga->g_iga->geometry && iga->g_iga->geometryX){
      if (PetscLikely(dim == nsd))
        IGA_GeometryMap_2D(order[fld],1,nen[fld],X,gbasis[0][0],gbasis[0][1],gbasis[0][2],gbasis[0][3],gbasis[0][4],
                                                   mapX[0]  ,mapX[1]  ,mapX[2]  ,mapX[3]  ,mapX[4]  );
      else
        IGA_GeometryMap(order[fld],dim,nsd,1,nen[fld],X,gbasis[0][0],gbasis[0][1],gbasis[0][2],gbasis[0][3],gbasis[0][4],
                                                        mapX[0]  ,mapX[1]  ,mapX[2]  ,mapX[3]  ,mapX[4]  );
    }

    //----------------------------------------------//
    // Geometry inverse mapping
    if (arrayX && PetscLikely(dim == nsd)) {
      IGA_InverseMap_2D(order[fld],1,mapX[1],mapX[2],mapX[3],mapX[4],detX,
                                     mapU[1],mapU[2],mapU[3],mapU[4]);
    }
  }

  //----------------------------------------------//
  // Shape functions
  PetscInt    nfld_curl = 2;    // Number of Hcurl fields (In this case is 2)
  PetscReal **N_curl    = NULL;
  // Allocate memory
  ierr  = PetscMalloc(sizeof(PetscReal *)*nfld_curl,&N_curl  );CHKERRQ(ierr);
  // Build curl basis functions
  N_curl[0] = fbasis[0][0];
  N_curl[1] = fbasis[2][0];
  // Build Mapping
  const PetscReal (*DFinv) [dim] = (typeof(DFinv))  mapU[1];
  // Shape functions
  PetscReal Nx[nen[0]];
  PetscReal Nz[nen[2]];
  for (i=0; i<nen[0]; i++) {
    Nx[i] = DFinv[0][0]*N_curl[0][i]+DFinv[1][0]*N_curl[1][i];
    Nz[i] = DFinv[0][1]*N_curl[0][i]+DFinv[1][1]*N_curl[1][i];
  }

  //----------------------------------------------//
  // Compute puntual value
  IGA_GetValue(nen[0],dof,Nx,A[0],&u[0]);
  IGA_GetValue(nen[0],dof,Nz,A[2],&u[1]);

  //----------------------------------------------//
  // Set u_io values
  u_io[0] = u[0];
  u_io[1] = 0.0 ;
  u_io[2] = u[1];

  //----------------------------------------------//
  // Free memory
  for (i=0; i<fld; i++) {
    lvec[i]   = NULL;
    arrayA[i] = NULL;
    A[i]      = NULL;
    for (j = 0; j < nsd; ++j){
      U[i][j] = NULL;
    }
    for (j = 0; j < ndiff; ++j){
      if (fbasis[i][j]!=NULL)
      {
        ierr = PetscFree(fbasis[i][j]);CHKERRQ(ierr);
        fbasis[i][j] = NULL;
      }
    }
    ierr = PetscFree(map[i]);CHKERRQ(ierr);
    map[i]=NULL;
  }  
  arrayW = NULL;
  arrayX = NULL;
  for (i=0; i<fld; i++) {
    for (j = 0; j < nsd; ++j){
      ierr = PetscFree(BD[i][j]);CHKERRQ(ierr);
      BD[i][j] = NULL;
    }
  }
  for (j = 0; j < nsd; ++j){
    U[i][j] = NULL;
  }
  ierr = PetscFree(map[fld]);CHKERRQ(ierr);
  map[fld]=NULL;
  ierr = PetscFree(W/*   */);CHKERRQ(ierr);
  W = NULL;
  ierr = PetscFree(X/*   */);CHKERRQ(ierr);
  X = NULL;
  for (j = 0; j < ndiff; ++j){
    if (gbasis[0][j]!=NULL)
    {
      ierr = PetscFree(gbasis[0][j]);CHKERRQ(ierr);
      gbasis[0][j] = NULL;
    }
    if (mapU[j]!=NULL)
    {
      ierr = PetscFree(mapU[j]);CHKERRQ(ierr);
      mapU[j] = NULL;
    }
    if (mapX[j]!=NULL)
    {
      ierr = PetscFree(mapX[j]);CHKERRQ(ierr);
      mapX[j] = NULL;
    }
  }
  ierr = PetscFree(detX);CHKERRQ(ierr);
  detX = NULL;

  return 0;
}

//----------------------------------------------------------------------------------------------------------------------------//
// Main code
#undef  __FUNCT__
#define __FUNCT__ "main"
int main(int argc, char *argv[]) {

  PetscLogDouble t0,t05,t1,t2,t25,t3,t35,t4,t5;
  
  IGAM iga,iga_lqi;
  PetscInt i,j,k,n;
  PetscInt nel,l,hn_qp;
  PetscErrorCode ierr;
  ierr = PetscInitialize(&argc,&argv,0,0);CHKERRQ(ierr);
  ierr = PetscTime(&t0);CHKERRQ(ierr);

  ierr =  PetscMemorySetGetMaximumUsage();CHKERRQ(ierr);

  //----------------------------------------------//
  // User parameters
  Params user;
  // Set Number of fields
  PetscInt fld = 3; // H(curl) x H1
  // Set dimension of the problem
  PetscInt dim = 2;
  // Initialize and set beta parameter of the Fourier discretization
  ierr = PetscOptionsGetInt(NULL,NULL,"-nbetas",&user.nbetas,NULL);CHKERRQ(ierr);
  // Set the number of elements
  ierr = PetscOptionsGetInt(NULL,NULL,"-nel",&nel,NULL);CHKERRQ(ierr);
  // Set the level od partitioning using C^0 separators
  ierr = PetscOptionsGetInt(NULL,NULL,"-l",&l,NULL);CHKERRQ(ierr);
  // Set the number of quadrature points for create the FE space used for source integration
  ierr = PetscOptionsGetInt(NULL,NULL,"-hn_qp",&hn_qp,NULL);CHKERRQ(ierr);
  // Set the Segment of the domain where we are located 
  ierr = PetscOptionsGetInt(NULL,NULL,"-sg",&user.sg,NULL);CHKERRQ(ierr);
  //
  // Set the Magnetic permeability
  PetscReal mu=0; // nu      -> magnetic permeability
  char string1[PETSC_MAX_PATH_LEN];
  ierr = PetscOptionsGetString(NULL,NULL,"-mu",string1,sizeof(string1),NULL);CHKERRQ(ierr);
  cv_str2num(string1,&mu);
  // Set the Electric permeability
  PetscReal epsilon=0; // epsilon -> electric permeability
  char string2[PETSC_MAX_PATH_LEN];
  ierr = PetscOptionsGetString(NULL,NULL,"-ep",string2,sizeof(string2),NULL);CHKERRQ(ierr);
  cv_str2num(string2,&epsilon);
  // set the frequency
  PetscReal omega=0; // omega   -> frequency
  char string3[PETSC_MAX_PATH_LEN];
  ierr = PetscOptionsGetString(NULL,NULL,"-om",string3,sizeof(string3),NULL);CHKERRQ(ierr);
  cv_str2num(string3,&omega);
  // Set the resistivity
  PetscReal sigma=0; // sigma   -> resistivity
  char string4[PETSC_MAX_PATH_LEN];
  ierr = PetscOptionsGetString(NULL,NULL,"-sm",string4,sizeof(string4),NULL);CHKERRQ(ierr);
  cv_str2num(string4,&sigma);
  // 
  PetscReal sigma_u=0; // sigma_u   -> resistivity of the upper layer
  char string41[PETSC_MAX_PATH_LEN];
  ierr = PetscOptionsGetString(NULL,NULL,"-smu",string41,sizeof(string41),NULL);CHKERRQ(ierr);
  cv_str2num(string41,&sigma_u);  
  // 
  PetscReal sigma_l=0; // sigma_l   -> resistivity of the lower layer
  char string42[PETSC_MAX_PATH_LEN];
  ierr = PetscOptionsGetString(NULL,NULL,"-sml",string42,sizeof(string42),NULL);CHKERRQ(ierr);
  cv_str2num(string42,&sigma_l);
  // 
  PetscReal afactor=0; // afactor   -> resistivity anisotropy factor sigma_v = afactor*sigma_h
  char string43[PETSC_MAX_PATH_LEN];
  ierr = PetscOptionsGetString(NULL,NULL,"-af",string43,sizeof(string43),NULL);CHKERRQ(ierr);
  cv_str2num(string43,&afactor);  
  // 
  PetscReal caseno=0; // caseno   -> the case number in Database generation for DL inversion
  char string0[PETSC_MAX_PATH_LEN];
  ierr = PetscOptionsGetString(NULL,NULL,"-cs",string0,sizeof(string0),NULL);CHKERRQ(ierr);
  char *csigma=string0;
  cv_str2num(string0,&caseno);  
  //
  // 
  // PetscReal sigmas[3] = {sigma,0.1*sigma,10.0*sigma};
  // PetscReal sigmas[3] = {50.0/3.0*sigma,sigma,50.0*sigma}; // Heterogeneous media, one geological fault, sigma = 0.02
  // PetscReal sigmas[3] = {50.0/3.0*sigma,0.5*sigma,100.0*sigma}; // Heterogeneous media, two geological faults, sigma = 0.02
  PetscReal sigmas[3] = {sigma,sigma_u,sigma_l}; // Database generation for DL inversion
  //
  // Set alpha source
  user.alpha = 10000;
  // Set domain zone containing the source radious
  user.r0 = 0.05;
  // Set tool orientation
  user.theta=0; // Database generation for DL inversion
  char string11[PETSC_MAX_PATH_LEN];
  ierr = PetscOptionsGetString(NULL,NULL,"-theta",string11,sizeof(string11),NULL);CHKERRQ(ierr);
  cv_str2num(string11,&user.theta);
  // 
  PetscReal step=0; // Moving steps of the tool (m)
  char string5[PETSC_MAX_PATH_LEN];
  ierr = PetscOptionsGetString(NULL,NULL,"-step",string5,sizeof(string5),NULL);CHKERRQ(ierr);
  cv_str2num(string5,&step);
  // 
  PetscReal xsrc=0; // x ccordinate of the src at the begining of the subtrajectory
  char string6[PETSC_MAX_PATH_LEN];
  ierr = PetscOptionsGetString(NULL,NULL,"-xsrc",string6,sizeof(string6),NULL);CHKERRQ(ierr);
  cv_str2num(string6,&xsrc);
  PetscReal zsrc=0; // z ccordinate of the src at the begining of the subtrajectory
  char string7[PETSC_MAX_PATH_LEN];
  ierr = PetscOptionsGetString(NULL,NULL,"-zsrc",string7,sizeof(string7),NULL);CHKERRQ(ierr);
  cv_str2num(string7,&zsrc);
  // 
  user.du=0; // Database generation for DL inversion
  char string8[PETSC_MAX_PATH_LEN];
  ierr = PetscOptionsGetString(NULL,NULL,"-du",string8,sizeof(string8),NULL);CHKERRQ(ierr);
  cv_str2num(string8,&user.du);
  // 
  user.dl=0; // Database generation for DL inversion
  char string9[PETSC_MAX_PATH_LEN];
  ierr = PetscOptionsGetString(NULL,NULL,"-dl",string9,sizeof(string9),NULL);CHKERRQ(ierr);
  cv_str2num(string9,&user.dl); 
  // 
  user.TL=0; // tool length in Database generation for DL inversion
  char string10[PETSC_MAX_PATH_LEN];
  ierr = PetscOptionsGetString(NULL,NULL,"-TL",string10,sizeof(string10),NULL);CHKERRQ(ierr);
  cv_str2num(string10,&user.TL);   
  //
  // 
  // Set the Direct solver package
  // strcpy(user.solver_type,"MUMPS");
  strcpy(user.solver_type,"PARDISO");
  //----------------------------------------------//
  // Paramaters to compute the solution
  user.mu       = mu;
  user.omega    = omega;
  user.sigma    = sigmas[0];
  user.step     = step;
  user.xsrc     = xsrc;
  user.zsrc     = zsrc;
  user.afactor  = afactor;

  //----------------------------------------------//
  // This section identify the element that contain the source
  // The cost of this section is linearly dependent of the number of elements
  IGA geom_lin;
  ierr = IGACreate(PETSC_COMM_WORLD,&geom_lin);CHKERRQ(ierr);
  ierr = IGASetDof(geom_lin,1);CHKERRQ(ierr);
  ierr = IGASetDim(geom_lin,dim);CHKERRQ(ierr);  
  ierr = IGASetOptionsPrefix(geom_lin,"geo_lin_");CHKERRQ(ierr);
  ierr = IGASetFromOptions(geom_lin);CHKERRQ(ierr);
  ierr = IGASetUp(geom_lin);CHKERRQ(ierr);

  // Set length of the domain in the y axis direction assuming we work on a cube
  PetscReal Ui,Uf;
  ierr = IGAAxisGetLimits(geom_lin->axis[0],&Ui,&Uf);CHKERRQ(ierr);
  user.Ly = Uf-Ui;

  //----------------------------------------------//
  // Set the parameters used to build the matrix system of the PDE
  // NOTE: I will improve this part converting it in elemental values, but it is future work
  PetscReal Cnt_Fmode = 0.0;
  PetscReal sigma_h;
  PetscReal sigma_v;
  user.mu_bar = (PETSC_i*mu*omega);
  for (i=0; i<3; i++)
  {
    if (i==0) {
      sigma_h = sigmas[i];              // sigma horizontal   -> electric conductivity
      sigma_v = sigmas[i]*user.afactor; // sigma vertical     -> electric conductivity
    }
    else {
      sigma_h = sigmas[i];
      sigma_v = sigmas[i];
    }
    user.sigma_bar[0][i] = (1.0/(sigma_h+PETSC_i*epsilon*omega));
    user.sigma_bar[1][i] = (1.0/(sigma_v+PETSC_i*epsilon*omega));
  }

  //----------------------------------------------//
  // In this section we create the fields required to perform a FE analysis
  IGA geom;
  ierr = IGACreate(PETSC_COMM_WORLD,&geom);CHKERRQ(ierr);
  ierr = IGASetDof(geom,1);CHKERRQ(ierr);
  ierr = IGASetDim(geom,dim);CHKERRQ(ierr);
  ierr = IGASetOptionsPrefix(geom,"geo_");CHKERRQ(ierr);
  IGA field[fld+1]; const char *prefix[] = {"Hx_","Hy_","Hz_"};
  for (i=0; i<fld; i++) {
    ierr = IGACreate(PETSC_COMM_WORLD,&field[i]);CHKERRQ(ierr);
    ierr = IGASetDof(field[i],1);CHKERRQ(ierr);
    ierr = IGASetDim(field[i],dim);CHKERRQ(ierr);
    ierr = IGASetOptionsPrefix(field[i],prefix[i]);CHKERRQ(ierr);
  }
  // Create copy of the fields FE spaces with higher number of quadrature points
  PetscInt lqi = 10;
  IGA geom_lqi;
  ierr = IGACreate(PETSC_COMM_WORLD,&geom_lqi);CHKERRQ(ierr);
  ierr = IGASetDof(geom_lqi,1);CHKERRQ(ierr);
  ierr = IGASetDim(geom_lqi,dim);CHKERRQ(ierr);
  ierr = IGASetOptionsPrefix(geom_lqi,"geo_");CHKERRQ(ierr);
  IGA field_lqi[fld+1];
  for (i=0; i<fld; i++) {
    ierr = IGACreate(PETSC_COMM_WORLD,&field_lqi[i]);CHKERRQ(ierr);
    ierr = IGASetDof(field_lqi[i],1);CHKERRQ(ierr);
    ierr = IGASetDim(field_lqi[i],dim);CHKERRQ(ierr);
    ierr = IGASetOptionsPrefix(field_lqi[i],prefix[i]);CHKERRQ(ierr);    
  }
  // Creation IGAM, object that contain the IGA fields
  ierr = IGAMCreate(PETSC_COMM_WORLD,&iga);CHKERRQ(ierr);
  ierr = IGAMCompose(iga,geom,fld,field);CHKERRQ(ierr);
  ierr = IGAMSetFromOptions(iga);CHKERRQ(ierr);
  ierr = IGAMSetUp(iga);CHKERRQ(ierr);
  // Creation IGAM, object that contain the IGA fields with larger quadrature integration
  ierr = IGAMCreate(PETSC_COMM_WORLD,&iga_lqi);CHKERRQ(ierr);
  ierr = IGAMCompose(iga_lqi,geom_lqi,fld,field_lqi);CHKERRQ(ierr);
  ierr = IGAMSetFromOptions(iga_lqi);CHKERRQ(ierr);
  for (j=0; j<dim; j++) {
    for (i=0; i<fld; i++) {
      ierr = IGASetQuadrature(field_lqi[i],j,lqi);CHKERRQ(ierr);      
    }
    ierr = IGASetQuadrature(geom_lqi,j,lqi);CHKERRQ(ierr);      
  }
  ierr = IGAMSetUp(iga_lqi);CHKERRQ(ierr);
  // Set of the Dirichlet boundary conditions
  PetscInt fldbnd[2] = {0,2}; 
  for (i=0; i<dim; i++) {
    PetscInt axis=i,side;
    for (side=0; side<2; side++) {
      ierr = IGAMSetBoundaryValue(iga,axis,side,fldbnd[1-i],0.0);CHKERRQ(ierr);
      ierr = IGAMSetBoundaryForm (iga,axis,side,PETSC_TRUE);CHKERRQ(ierr);
      // Set boundaries to IGAM lqi
      ierr = IGAMSetBoundaryValue(iga_lqi,axis,side,fldbnd[1-i],0.0);CHKERRQ(ierr);
      ierr = IGAMSetBoundaryForm (iga_lqi,axis,side,PETSC_TRUE);CHKERRQ(ierr);
    }
  }     

  //----------------------------------------------//
  // Set the number of discrete spaces (In this case 2 1) Hcurl 2) H1)
  iga->numds = 2;
  // H(curl) space
  PetscInt curl_flds[] = {0,2};
  ierr = IGAMSetDS(iga,0,CURL_CONFORMING,2,curl_flds);CHKERRQ(ierr);
  // H1 space
  PetscInt H1_flds[] = {1};
  ierr = IGAMSetDS(iga,1,H1_CONFORMING,1,H1_flds);CHKERRQ(ierr);
  //Must be called after IGAMSetUp()
  ierr = IGAMDSElementInit(iga);CHKERRQ(ierr);
  // Order degree
  PetscInt p,ord=100;
  IGAAxis  axis;
  ierr = IGAGetAxis(field[1],0,&axis);CHKERRQ(ierr);
  ierr = IGAAxisGetDegree(axis,&p);CHKERRQ(ierr);
  ord  = PetscMin(ord,p);

  //----------------------------------------------//
  // Set the number of discrete spaces (In this case 2 1) Hcurl 2) H1) for IGAM lqi
  iga_lqi->numds = 2;
  // H(curl) space
  ierr = IGAMSetDS(iga_lqi,0,CURL_CONFORMING,2,curl_flds);CHKERRQ(ierr);
  // H1 space
  ierr = IGAMSetDS(iga_lqi,1,H1_CONFORMING,1,H1_flds);CHKERRQ(ierr);
  //Must be called after IGAMSetUp()
  ierr = IGAMDSElementInit(iga_lqi);CHKERRQ(ierr);

  //----------------------------------------------//
  // Vector
  iga_lqi->form->ops->Vector = VectorRHS;
  iga_lqi->form->ops->VecCtx = &user;

  //----------------------------------------------//
  // Initialize the helmholtz system
  Vec x,b;
  ierr = IGAMCreateVec(iga,&x);CHKERRQ(ierr);
  ierr = IGAMCreateVec(iga_lqi,&b);CHKERRQ(ierr);

  //----------------------------------------------//
  // Initialize fill array
  PetscInt fill[fld][fld];

  //----------------------------------------------//
  // Initialize matrix sigma_bar + mu_bar
  Mat Asm_bar;
  PetscArrayzero(&fill[0][0],fld*fld);
  fill[0][0] = 1;               fill[0][2] = 1;
                 fill[1][1] = 1;
  fill[2][0] = 1;               fill[2][2] = 1;
  ierr = IGAMSetCoupling(iga,&fill[0][0]);CHKERRQ(ierr);
  ierr = IGAMCreateMat(iga,&Asm_bar);CHKERRQ(ierr);  

  //----------------------------------------------//
  // Initialize Partial matrices
  // Matrix A_sigma_bar
  Mat A_sigma_bar;
  iga->form->ops->Matrix = MatrixLHS_sigma_bar;
  iga->form->ops->MatCtx = &user;
  PetscArrayzero(&fill[0][0],fld*fld);
  fill[0][0] = 1;               fill[0][2] = 1;
                 fill[1][1] = 1;
  fill[2][0] = 1;               fill[2][2] = 1;
  ierr = IGAMSetCoupling(iga,&fill[0][0]);CHKERRQ(ierr);
  ierr = IGAMCreateMat(iga,&A_sigma_bar);CHKERRQ(ierr);
  ierr = IGAMComputeMatrix(iga,A_sigma_bar);CHKERRQ(ierr);
  ierr = MatAXPY(Asm_bar,1.0,A_sigma_bar,SAME_NONZERO_PATTERN);CHKERRQ(ierr);
  ierr = MatDestroy(&A_sigma_bar);CHKERRQ(ierr);
  // Matrix A_mu_bar
  Mat A_mu_bar;
  iga->form->ops->Matrix = MatrixLHS_mu_bar;
  iga->form->ops->MatCtx = &user;
  PetscArrayzero(&fill[0][0],fld*fld);
  fill[0][0] = 1;
                 fill[1][1] = 1;
                                fill[2][2] = 1;
  ierr = IGAMSetCoupling(iga,&fill[0][0]);CHKERRQ(ierr);
  ierr = IGAMCreateMat(iga,&A_mu_bar);CHKERRQ(ierr);
  ierr = IGAMComputeMatrix(iga,A_mu_bar);CHKERRQ(ierr);
  ierr = MatAXPY(Asm_bar,user.mu_bar,A_mu_bar,SUBSET_NONZERO_PATTERN );CHKERRQ(ierr);
  ierr = MatDestroy(&A_mu_bar);CHKERRQ(ierr);

  //----------------------------------------------//
  // Initialize matrix beta linear
  Mat A_beta_linear;
  iga->form->ops->Matrix = MatrixLHS_beta_linear;
  iga->form->ops->MatCtx = &user;
  PetscArrayzero(&fill[0][0],fld*fld);
                 fill[0][1] = 1;
  fill[1][0] = 1;               fill[1][2] = 1;
                 fill[2][1] = 1;
  ierr = IGAMSetCoupling(iga,&fill[0][0]);CHKERRQ(ierr);
  ierr = IGAMCreateMat(iga,&A_beta_linear);CHKERRQ(ierr);
  ierr = IGAMComputeMatrix(iga,A_beta_linear);CHKERRQ(ierr);

  //----------------------------------------------//
  // Initialize matrix beta quadratic
  Mat A_beta_quadratic;
  iga->form->ops->Matrix = MatrixLHS_beta_quadratic;
  iga->form->ops->MatCtx = &user;
  PetscArrayzero(&fill[0][0],fld*fld);
  fill[0][0] = 1;
                 
                                fill[2][2] = 1;
  ierr = IGAMSetCoupling(iga,&fill[0][0]);CHKERRQ(ierr);
  ierr = IGAMCreateMat(iga,&A_beta_quadratic);CHKERRQ(ierr);
  ierr = IGAMComputeMatrix(iga,A_beta_quadratic);CHKERRQ(ierr);

  //----------------------------------------------//
  // Initialize Global matrix
  Mat A;
  ierr = IGAMSetCoupling(iga,NULL);CHKERRQ(ierr);
  ierr = IGAMCreateMat(iga,&A);CHKERRQ(ierr);

  //----------------------------------------------//
  // Print Initialization time
  ierr = PetscTime(&t05);CHKERRQ(ierr);
  // ierr = PetscPrintf(PETSC_COMM_WORLD,"  ====================================================\n");CHKERRQ(ierr);
  // ierr = PetscPrintf(PETSC_COMM_WORLD,"    Initial Setup time:     %f sec\n",t05-t0);CHKERRQ(ierr);
  // ierr = PetscPrintf(PETSC_COMM_WORLD,"  ====================================================\n");CHKERRQ(ierr);

  //----------------------------------------------//
  // Geosteering tool with 2 sources.
  //
  //           Tool:  |S2----R2----R1----S1-|>
  //
  //          -- Tool Geometry --
  // Number of Sources
  PetscInt n_sr = 2;
  // Number of Receive
  PetscInt n_rc = 2;
  // Tool configuration: 
  PetscReal d_rc_sr[n_sr][n_rc];
  // Distance j-th source (S_j) -- id-th receiver (R_id) 
  d_rc_sr[0][0] =  0.466725; // Distance source (S1) -- receiver (R1)   * note: 1st receiver when S1 is the reference point
  d_rc_sr[0][1] =  0.669925; // Distance source (S1) -- receiver (R2)   * note: 2nd receiver when S1 is the reference point
  d_rc_sr[1][0] = -0.466725; // Distance source (S2) -- receiver (R1)   * note: 1st receiver when S2 is the reference point
  d_rc_sr[1][1] = -0.669925; // Distance source (S2) -- receiver (R2)   * note: 2nd receiver when S2 is the reference point
  // distance of sources
  PetscReal dsr = 1.13665;   // Distance source (S1) -- source (S2) = 0.466725 + 0.669925
  // 
  PetscReal source_mag[3];
  PetscReal source_loc[3];
  //
  //           -- Source data --
  // Initialize
  user.sr_id = 0;
  for (i=0;i<3;i++){ierr = PetscMalloc(sizeof(PetscReal*)*n_sr,&user.srcmag[i]);CHKERRQ(ierr);
                    ierr = PetscMalloc(sizeof(PetscReal*)*n_sr,&user.srcloc[i]);CHKERRQ(ierr);}
  ierr = PetscMalloc(sizeof(PetscInt*)*n_sr,&user.srcelem);CHKERRQ(ierr);
  ierr = PetscMalloc(sizeof(PetscInt*)*pow(nel,dim),&user.srczone);CHKERRQ(ierr);

  // Source # 1
  user.sr_id = 0;                    
  // Magnitude sources # 1
  source_mag[0] =  0.0;
  source_mag[1] =  0.0;
  source_mag[2] =  1.0;
  rot_mat(source_mag,user.theta);
  set_values(source_mag,user.srcmag,user.sr_id);
  // Source localization # 1
  source_loc[0] =  user.xsrc;
  source_loc[1] =  0.0;
  source_loc[2] =  user.zsrc;
  set_values(source_loc,user.srcloc,user.sr_id);

  // Source # 2
  user.sr_id = 1;
  // Magnitude sources # 2
  source_mag[0] =  0.0;
  source_mag[1] =  0.0;
  source_mag[2] =  1.0;
  rot_mat(source_mag,user.theta);
  set_values(source_mag,user.srcmag,user.sr_id);
  // Source localization # 2
  PetscReal drc[3];
  drc[0] = 0.0;
  drc[2] = dsr;
  rot_mat(drc,user.theta); // we need to first rotate the distance vector and then add it to srcloc
  source_loc[0] =  user.xsrc+drc[0];
  source_loc[1] =  0.0;
  source_loc[2] =  user.zsrc+drc[2];
  set_values(source_loc,user.srcloc,user.sr_id);

  // Identify the element what contains the source
  ierr = Get_ele_src(geom_lin,n_sr,&user);CHKERRQ(ierr);

  // Identify te zone that contains the source
  ierr = Get_zone_src(geom_lin,n_sr,&user);CHKERRQ(ierr);

  //----------------------------------------------//
  // Print Initialization time
  ierr = PetscTime(&t1);CHKERRQ(ierr);
  // ierr = PetscPrintf(PETSC_COMM_WORLD,"  ====================================================\n");CHKERRQ(ierr);
  // ierr = PetscPrintf(PETSC_COMM_WORLD,"    Find source elements time:     %f sec\n",t1-t05);CHKERRQ(ierr);
  // ierr = PetscPrintf(PETSC_COMM_WORLD,"  ====================================================\n");CHKERRQ(ierr);

  //----------------------------------------------//
  // Extra parameters to compute the receivers data
  // Receiver location
  PetscReal point[3];
  // Receiver solution
  PetscScalar u[3];

  //----------------------------------------------//
  // Extra parameters to compute Hzz and save data
  // Real and imaginary part
  PetscReal ReH_u[3];
  PetscReal ImH_u[3];
  // Hzz
  PetscReal ReHzz_u;
  PetscReal ImHzz_u;
  // |Hzz|
  PetscReal absHz_u; 
  // loop peasurement points id
  PetscInt id;

  //----------------------------------------------//
  // Loop of betas
  n = 0;
  Mat LU; KSP ksp; PC pc;
  for (n=0;n<user.nbetas;n++)
  {

    ierr = PetscTime(&t0);CHKERRQ(ierr);

    // Set beta value
    user.beta = n;
    // Set Coefficient
    Cnt_Fmode = user.beta*2.0*PETSC_PI/user.Ly;
    // Zeros Matrix A
    ierr = MatZeroEntries(A);CHKERRQ(ierr);

    //----------------------------------------------//
    // Assemble Matrix
    ierr = MatAXPY(A,1.0,Asm_bar,SUBSET_NONZERO_PATTERN );CHKERRQ(ierr);
    ierr = MatAXPY(A,Cnt_Fmode*PETSC_i,A_beta_linear,SUBSET_NONZERO_PATTERN );CHKERRQ(ierr);
    ierr = MatAXPY(A,pow(Cnt_Fmode,2),A_beta_quadratic,SUBSET_NONZERO_PATTERN );CHKERRQ(ierr);

    ierr = PetscTime(&t05);CHKERRQ(ierr);
  

    //----------------------------------------------//
    // Factorization of the Matrix using LU factorization
    if (n==0) // initial step in the beta's loop
    {
      // Create KSP
      ierr = IGAMCreateKSP(iga,&ksp);CHKERRQ(ierr);
      ierr = KSPSetOperators(ksp,A,A);CHKERRQ(ierr);
      // Direct solver
      ierr = KSPSetType(ksp,KSPPREONLY);CHKERRQ(ierr);
      ierr = KSPGetPC(ksp,&pc);CHKERRQ(ierr);
      ierr = PCSetType(pc,PCLU);CHKERRQ(ierr);
      // Solver type
      PetscInt MUMPS_type   = strcmp(user.solver_type,"MUMPS");
      PetscInt PARDISO_type = strcmp(user.solver_type,"PARDISO");
      if ( (MUMPS_type != 0) && (PARDISO_type != 0) ) SETERRQ1(PETSC_COMM_WORLD,1,"Incorrect direct solver type",NULL);
      if (MUMPS_type == 0)
      {
        ierr = PCFactorSetMatSolverType(pc,MATSOLVERMUMPS);CHKERRQ(ierr);
        ierr = PCFactorSetUpMatSolverType(pc);CHKERRQ(ierr); /* call MatGetFactor() to create LU */
        ierr = PCFactorGetMatrix(pc,&LU);CHKERRQ(ierr);
        ierr = MatMumpsSetIcntl(LU,7,5);CHKERRQ(ierr);
        // ierr = MatMumpsSetIcntl(LU,4,3);CHKERRQ(ierr);
      }
      else if (PARDISO_type == 0)
      {
        ierr = PCFactorSetMatSolverType(pc,MATSOLVERMKL_PARDISO);CHKERRQ(ierr);
        ierr = PCFactorSetUpMatSolverType(pc);CHKERRQ(ierr); /* call MatGetFactor() to create LU */
        ierr = PCFactorGetMatrix(pc,&LU);CHKERRQ(ierr);
        //ierr = MatMkl_PardisoSetCntl(LU,68,1);CHKERRQ(ierr);
      }
      // Setup solver
      ierr = KSPSetFromOptions(ksp);CHKERRQ(ierr);
      ierr = KSPSetUp(ksp);CHKERRQ(ierr);
    }
    else
    {
      ierr = KSPSetOperators(ksp,A,A);CHKERRQ(ierr);
    }

    ierr = PetscTime(&t2);CHKERRQ(ierr);

    //
    // ierr = PetscPrintf(PETSC_COMM_WORLD,"  ====================================================\n");CHKERRQ(ierr);
    // ierr = PetscPrintf(PETSC_COMM_WORLD,"    solving for beta:                  %d out of %d\n",n,user.nbetas-1);CHKERRQ(ierr);
    // ierr = PetscPrintf(PETSC_COMM_WORLD,"    loop Setup time:                   %f sec\n",t05-t0);CHKERRQ(ierr);
    // ierr = PetscPrintf(PETSC_COMM_WORLD,"    loop Set solver time:              %f sec\n",t2-t1) ;CHKERRQ(ierr);
    //

    // Loop of local positions of the tool in the n-th single mesh
    i = 0; // Position of the tool in a single mesh

    // loop of sources
    j = 0; // Active source
    for (j=0;j<n_sr;j++)
    {

      ierr = PetscTime(&t25);CHKERRQ(ierr);

      // set source id
      user.sr_id = j;

      // Compute RHS for the j-th source
      ierr = IGAMComputeRHS(iga_lqi,b,&user);CHKERRQ(ierr);      

      ierr = PetscTime(&t3);CHKERRQ(ierr);

      //----------------------------------------------//
      // Compute solution using previous computed RHS
      ierr = KSPSolve(ksp,b,x);CHKERRQ(ierr);

      ierr = PetscTime(&t35);CHKERRQ(ierr);

      
      //----------------------------------------------//
      // Open file to save Hzz solution
      char filename[PETSC_MAX_PATH_LEN];
      FILE           *file = NULL;
      // Create file name
      PetscSNPrintf(filename,sizeof(filename),"Hzz/Hzz_N%d_p%d_l%d_bt%d_sg%d_Pos%d_Src%d_sigma%s.txt",nel,ord,l,user.beta,user.sg,i+1,j+1,csigma);
      // open file and write header
      file = fopen(filename,"w");
      if (!file) SETERRQ1(PETSC_COMM_WORLD,PETSC_ERR_SYS,"Cannot open file = %s \n",filename);
      PetscFPrintf(PETSC_COMM_WORLD,file," Point(x,z)       | source(x,z)       | Souce magnitude(x,y,z)      | Real(u)      | Imag(u)       | Mag(u)\n");

      ierr = PetscTime(&t4);CHKERRQ(ierr);

      //----------------------------------------------//
      // id-th measurement point
      for (id=0;id<n_rc;id++)
      {
        // This line is for id-th point of measurement at tool S2----R2----R1----S1- >
        PetscReal drc[3];
        drc[0] = 0.0;
        drc[2] = d_rc_sr[j][id];
        rot_mat(drc,user.theta); // we need to first rotate the distance vector and then add it to srcloc
        // 
        point[0] = user.srcloc[0][j]+drc[0];//+delta;
        point[2] = user.srcloc[2][j]+drc[2];
        // Reading data
        ierr   = GetRValue(iga,fld,x,point,u);CHKERRQ(ierr);
        // Extract real and imaginary part
        for (k=0;k<3;k++){ReH_u[k] = PetscRealPart(u[k]); ImH_u[k] = PetscImaginaryPart(u[k]);}
        // Computation of Hzz
        rot_mat(ReH_u,user.theta);
        rot_mat(ImH_u,user.theta);
        ReHzz_u = ReH_u[2];
        ImHzz_u = ImH_u[2];

        // Compute |Hzz|
        absHz_u = sqrt(pow(ReHzz_u,2)+pow(ImHzz_u,2));
        // Saving data
        PetscFPrintf(PETSC_COMM_WORLD,file,"%f %f | %f %f | %f %f %f | %e | %e | %e\n",point[0],point[2],
                                                                                       user.srcloc[0][j],user.srcloc[2][j],
                                                                                       user.srcmag[0][j],user.srcmag[1][j],user.srcmag[2][j],
                                                                                       ReHzz_u,ImHzz_u,absHz_u);
      }
      // Close file
      if (file) {fclose(file); file = NULL;}

      ierr = PetscTime(&t5);CHKERRQ(ierr);

    }
  }

  //----------------------------------------------//
  // Cleanup
  ierr = MatDestroy(&A);CHKERRQ(ierr);
  ierr = VecDestroy(&b);CHKERRQ(ierr);
  ierr = KSPDestroy(&ksp);CHKERRQ(ierr);
  ierr = PetscFinalize();CHKERRQ(ierr);

  return 0;
}
